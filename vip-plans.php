<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
      <!-- build:css css/animate.min.css -->
    <link rel="stylesheet" href="dev/css/animate.css">
    <!-- endbuild -->
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="">
    <div class="sticky-content">
        <?php include "dev/php/header-is-not-login.php"; ?>

        <!--    step 1 VIP方案選擇       -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">方案<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">CWMoney VIP 序號兌換</h2>
                            <button class="btn btn-coupon">點擊兌換</button>
                            <span class="label--corner"> <span class="animated tada">開運特刊</span></span>
                        </div>
                    </div>
                    <!--      for future vip plans              -->
                    <div class="col-sm-6 hidden">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">CWMoney VIP 序號兌換</h2>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /. sticky-content -->
    <?php include "dev/php/footer.php"; ?>

    
    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
