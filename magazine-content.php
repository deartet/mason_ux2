<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
  <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->
</head>

<body id="">
    <div class="sticky-content">

        <section class="magazine-toolbar fixed-top">
            <div class="container-fluid">
                <div class="pull-left">
                    <div class="magazine-toolbar__backToList"><a href="#"><span class="icon icon--backarrow"></span>回到雜誌列表</a></div>
                    <a href="#" class="zoom-in text-hide">點擊放大</a>
                    <a href="#" class="zoom-out text-hide">點擊縮小</a>
                </div>
                <div class="pull-right">
                    <select class="input-sm">
                        <option value="" disabled="disabled" selected="selected">請選擇頁數</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
        </section>




        <div class="dummy">
            <img src="/dev/images/img_magazine-dummy.png" style="width: 100%; margin-top: 80px;" alt="">
        </div>







    </div>
    <!-- /. sticky-content -->


    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
