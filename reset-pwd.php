<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="signup">
    <div class="sticky-content">
        <?php include "dev/php/header-is-not-login.php"; ?>
        
        <form class="form-horizontal l-signup signup">
            <div class="login_heading-container my-5">
                <div class="login_heading">重新取得密碼</div>
            </div>
            
            <div class="form-group has-feedback">
                <div class="col-sm-12">
                    <div class="input-group my-3">
                        <span class="input-group-addon"><span class="icon icon--email"></span></span>
                        <input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" placeholder="請輸入email地址">
                        <!--
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess2Status" class="sr-only">(success)</span>
-->
                    </div>
                </div>

            </div>
        
            

            <div class="form-group my-3">
                <button type="submit" class="btn btn-default btn-block btn-lg btn-reset-pwd">重置密碼</button>
            </div>
          


            <div class="form-group">
                <div class="founder">Powered by Money.com.tw</div>
            </div>
        </form>
        
    </div>
    <!-- /. sticky-content -->
    <?php include "dev/php/footer.php"; ?>

 
    <!-- build:js js/jquery.min.js -->
     <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
