<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="signup">
    <div class="sticky-content">
        <?php include "dev/php/header-is-not-login.php"; ?>
        
        <form class="form-horizontal l-signup signup">
            <div class="login_heading-container">
                <div class="login_heading">註冊</div>
            </div>
            <div class="form-group social-login-container">
                <button type="button" class="btn btn-facebook btn-lg btn-block"><span class="icon icon--facebook"></span><span class="division--vertical"></span><span class="va-middle">使用facebook帳號註冊</span></button>
                <button type="button" class="btn btn-gmail btn-lg btn-block">
                    <span class="mx-button">
                        <span class="icon icon--gmail"></span><span class="division--vertical"></span><span class="va-middle">使用Gmail帳號註冊</span>
                    </span>
                </button>
            </div>
            <div class="form-group">
                <div class="two-sides-divider_container">
                    <p class="two-sides-divider__text"> <span class="rounded">或</span></p>
                    <span class="two-sides-divider__line"></span>
                </div>
            </div>
            <div class="form-group has-feedback">
                <div class="col-sm-12">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="icon icon--email"></span></span>
                        <input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" placeholder="請輸入email地址">
                        <!--
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess2Status" class="sr-only">(success)</span>
-->
                    </div>
                </div>

            </div>
            <div class="form-group has-feedback">
                <div class="col-sm-12">
                    <div class="input-group">
                        <span class="input-group-addon addon-pwd"><span class="icon icon--password"></span></span>
                        <input type="text" class="form-control" id="inputGroupSuccess3" aria-describedby="inputGroupSuccess2Status" placeholder="請輸入密碼">
                        <!--
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess2Status" class="sr-only">(success)</span>
-->
                    </div>
                </div>
            </div>
              <div class="form-group has-feedback my-5">
                <div class="col-sm-12">
                    <div class="input-group">
                        <span class="input-group-addon addon-pwd"><span class="empty-icon"></span></span>
                        <input type="text" class="form-control" id="inputGroupSuccess4" aria-describedby="inputGroupSuccess2Status" placeholder="請再次輸入密碼">
                        <!--
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess2Status" class="sr-only">(success)</span>
-->
                    </div>
                </div>
            </div>
            

            <div class="form-group">
                <button type="submit" class="btn btn-default btn-block btn-lg btn-register--secondary">註冊 Money 會員</button>
            </div>
            <div class="form-group text-center is-not-member">
                <p>已註冊？<a href="#" class="signUp">請登入</a></p>
            </div>


            <div class="form-group">
                <div class="founder">Powered by Money.com.tw</div>
            </div>
        </form>  
    </div>
    <!-- /. sticky-content -->
    <?php include "dev/php/footer.php"; ?>

  
    <!-- build:js js/jquery.min.js -->
     <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
