<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!-- endbuild -->
</head>

<body id="insurance-overview">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container fix-little-content-width ">
            <div class="row login_first-row">
                <div class="col-md-8">
                    <section class="o_content-box insurance-profile">
                        <div class="insurance-profile__banner insurance-profile--top">
                            <div class="insurance-profile__banner-text">
                                <p>把保險與愛 一起裝進口袋</p>
                                <p>保險大小事，在這裡一次解決</p>
                            </div>
                        </div>
                        <div class="vault-profile--bottom">
                            <div class="vault-profile__photo">
                                <a href="#"><img src="/dev/images/img_profile.png" alt=""></a>
                            </div>
                            <div class="vault-profile__name">Martina Hingis</div>
                        </div>
                    </section>
                    <section class="o_content-box insurance-total">
                        <h1 class="m_heading1"><span class="icon icon--total-asset"></span><span class="m_heading1__title">保險總覽</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                        <div class="insurance-hot-product-query">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">熱門商品查詢</span></h2>
                            <div class="insurance-search">
                                <div class="input-group input-group-search insurance-search__input">
                                    <input type="text" class="form-control" placeholder="輸入商品名稱/代號/保險公司/險種/...">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default btn-insurance" type="button"><span class="icon icon--search" style="position: relative; top: -1px;"></span></button>
                                    </span>
                                </div>
                                <div class="insurance-search__hot-label">
                                    熱門查詢：
                                </div>
                                <ul class="insurance-search__hot-hastag clearfix">
                                    <li><a href="#">#小額終老保險</a></li>
                                    <li><a href="#">#定期壽險</a></li>
                                    <li><a href="#">#實支實付醫療險</a></li>
                                    <li><a href="#">#癌症險</a></li>
                                    <li><a href="#">#安達人壽</a></li>
                                    <li><a href="#">#癌症險</a></li>
                                    <li><a href="#">#安達人壽</a></li>
                                    <li><a href="#">#三商美邦加倍終身壽險</a></li>
                                </ul>
                            </div>

                            <div class="table-insurance-overview-container">
                                <div class="table-responsive scrolling-hints">
                                    <table class="table table-hover table-insurance-overview">
                                        <thead>
                                            <tr>
                                                <th colspan="5">C/P值最高</th>
                                            </tr>
                                            <tr>
                                                <th>險種</th>
                                                <th>公司</th>
                                                <th>保險名稱</th>
                                                <th>CP指數</th>
                                                <th>商品說明</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>醫療險（實支）</td>
                                                <td>法國巴黎人壽</td>
                                                <td>享安住院醫療健康保險附約醫療健康保險附約</td>
                                                <td>50</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>90</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>100</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" class="btn-ghost--insurance--round">查看更多</button>
                            </div>
                            <div class="table-insurance-overview-container">
                                <div class="table-responsive scrolling-hints">
                                    <table class="table table-hover table-insurance-overview">
                                        <thead>
                                            <tr>
                                                <th colspan="5">最多人買</th>
                                            </tr>
                                            <tr>
                                                <th>險種</th>
                                                <th>公司</th>
                                                <th>保險名稱</th>
                                                <th>購買人次</th>
                                                <th>商品說明</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>醫療險（實支）</td>
                                                <td>法國巴黎人壽</td>
                                                <td>享安住院醫療健康保險附約醫療健康保險附約</td>
                                                <td>50</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>90</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>100</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" class="btn-ghost--insurance--round">查看更多</button>
                            </div>

                            <div class="table-insurance-overview-container">
                                <div class="table-responsive scrolling-hints">
                                    <table class="table table-hover table-insurance-overview">
                                        <thead>
                                            <tr>
                                                <th colspan="5">最多人缺</th>
                                            </tr>
                                            <tr>
                                                <th>險種</th>
                                                <th>公司</th>
                                                <th>保險名稱</th>
                                                <th>缺少數量</th>
                                                <th>商品說明</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>醫療險（實支）</td>
                                                <td>法國巴黎人壽</td>
                                                <td>享安住院醫療健康保險附約醫療健康保險附約</td>
                                                <td>50</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>90</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                            <tr class="js-table-row--new-page" data-table-row="new-page">
                                                <td>健康險</td>
                                                <td>法國巴黎人壽</td>
                                                <td>一年定期重大疾病健康保險</td>
                                                <td>100</td>
                                                <td><button type="button" class="btn-ghost--insurance--rec">查閱</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" class="btn-ghost--insurance--round">查看更多</button>
                            </div>
                        </div>
                        <!-- end hot product query-->

                        <section class="insurance-feature-article-container">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">保險文章精選</span><a href="#" class="content-more">更多<i>»</i></a></h2>
                            <div class="insurance-feature-article flex-row">
                                <a href="#" class="insurance-feature-article__img flex-col-sm-5"></a>
                                <div class="insurance-feature-article__text flex-col-sm-7">
                                    <header>
                                        <h3 class="heading3"><a href="#">受傷致殘 勞保賠、意外險不賠？</a></h3>
                                        <p class="heading3-description">下樓梯時跌倒受傷致殘，看起來純屬意外狀況，保戶順利申請到勞保理賠，但在申請意外險時卻被拒絕</p>
                                    </header>
                                    <div class="meta">瀏覽人次：37240</div>
                                </div>
                            </div>
                        </section>

                        <section class="insurance-video-container">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">保險小學堂精選</span><a href="#" class="content-more">更多<i>»</i></a></h2>
                            <!-- 4:3 aspect ratio -->
                            <div class="insurance-video-box">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rb8jxKrAzzI"></iframe>
                                </div>
                                <header>
                                    <p class="heading3-description">保險小學堂第一課</p>
                                    <h3 class="heading3"><a href="#">退休金破產，每月5萬長照費有救嗎？</a></h3>

                                </header>
                            </div>
                            <div class="insurance-video-box">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YG_NAkYHGJk"></iframe>
                                </div>
                                <header>
                                    <p class="heading3-description">保險小學堂第二課</p>

                                    <h3 class="heading3"><a href="#">受傷致殘 勞保賠、意外險不賠？</a></h3>
                                </header>
                            </div>
                        </section>

                        <!-- Slider main container -->
                        <section class="swiper-container testimonial">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <div class="swiper-slide">
                                    <div class="testimonial__img">

                                    </div>
                                    <div class="testimonial__text">
                                        <div class="testimonial__name">Claire Chen</div>
                                        <div class="testimonial__title">美妝師</div>
                                        <div class="testimonial__quote">
                                            “以前自己的保險有不同公司的好幾家，每次都搞不清楚繳了哪張，哪張沒繳，幸好找到了保險管家，不但管理了我的保險，還貼心整理了我的保障，期待保險管家能越來越好！”
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">

                                    <div class="testimonial__img">

                                    </div>
                                    <div class="testimonial__text">
                                        <div class="testimonial__name">Winnie Wang</div>
                                        <div class="testimonial__title">PM</div>
                                        <div class="testimonial__quote">
                                            “以前自己的保險有不同公司的好幾家，每次都搞不清楚繳了哪張，哪張沒繳，幸好找到了保險管家，不但管理了我的保險，還貼心整理了我的保障，期待保險管家能越來越好！”
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>

                            <!-- If we need navigation buttons -->
                            <!--
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
-->
                        </section>

                        <section class="insurance-download-support-container">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">下載</span></h2>
                                    <div class="insurance-download">
                                        <div class="insurance-download__img">
                                            <h3 class="heading3">保險管家</h3>
                                            <p>聰明管理你的保險資產</p>
                                        </div>
                                        <div class="insurance-download__text">
                                            <h3 class="heading3">把保險與愛 裝進口袋</h3>
                                            <p>保險管家提供保單掃描OCR文字辨識功能， 貼心的幫您彙整保障內容，繳費通知提醒， 有關保險大小事，就交給保險管家！
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">支援</span></h2>
                                    <div class="insurance-support">
                                        <ul>
                                            <li><a href="mailto:service@cwmoney.net"><span class="icon icon--support"></span>錢管家客服</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        
                        <section class="insurance-footernote">
                            <p>合作夥伴：安睿保險經紀人</p>
                            <p>本站之保險商品資訊來自保發中心，各保險公司網站整理而得，僅提供民眾選購商品時參考，詳細及最新資訊請以官方文件為主。</p>
                        </section>
                    </section>
                    <!-- end insurance total -->
                </div>
                <div class="col-md-4">
                    <div class="o_content-box">
                        <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">活動通知</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                        <!-- Slider main container -->
                        <section class="swiper-container insurance-activity">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                <div class="swiper-slide">
                                    <a href="#">
                                        <img src="/dev/images/img_insurance-activity-2.png" alt="">
                                    </a>
                                    <h2 class="insurance-activity__heading">台中場 12/2 免費保險規劃講座</h2>
                                </div>
                                <div class="swiper-slide">
                                     <a href="#">
                                        <img src="/dev/images/img_insurance-activity-2.png" alt="">
                                    </a>
                                    <h2 class="insurance-activity__heading">台中場 1/25 免費保險規劃講座</h2>

                                </div>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination"></div>

                            <!-- If we need navigation buttons -->
                            <!--
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
-->
                        </section>
                    </div>
                    <div class="o_content-box">
                        <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">保險新知</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                        <div class="media-container media--full-height">
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">iPhoneX 「不慎手滑」...須拿 9188 元維修！</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">真實案例：雙薪夫妻年繳保費 35 萬！買儲蓄險存錢...
                                        </h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-10.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">退休怎麼保 ? 專家建議：以 10 年為基準、善加規畫 !</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-11.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">出國發生意外，看醫生 只能「自費」認栽 ?</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-12.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">全台女性平均 30 歲才嫁 ! 將面臨 3 怕的晚婚族們</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-circle hidden-md" data-toggle="modal" data-target="#myModal">
                  
                    </button>

                    <!-- Modal -->
                    <div class="modal fade slide-from-btm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="track-spending--mobile">
                                    <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">快速記帳</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </h1>

                                    <div class="track-spending_container">
                                        <form class="form-horizontal track-spending-input my-3">
                                            <div class="form-group">
                                                <label for="spend-date" class="col-md-2 control-label">日期</label>
                                                <div class="col-md-10">
                                                    <input type="text" style="min-width:96%" class="form-control" id="spend-date" placeholder="請輸入日期">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="spend-money" class="col-md-2 control-label">金額</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" id="spend-money" placeholder="請輸入金額">
                                                </div>
                                            </div>
                                            <div class="form-group my-3">
                                                <label for="spend-category" class="col-md-2 control-label">分類</label>
                                                <div class="col-md-10">
                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">食品酒水</option>
                                                                        <option value="2">菸酒茶飲料</option>
                                                                        <option value="3">運動健身</option>
                                                                        <option value="4">行動交通</option>
                                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="button" class="btn btn-track-spending">確認記帳</button>
                                                </div>
                                            </div>
                                        </form>
                                        <table class="table track-spending-table">
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-food.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">菸酒茶飲料 / $60</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-fun.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">運動健身 / $1600</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-traffic.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">行動交通 / $70</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <td colspan="2"><a href="#" class="content-more">更多 <i>&raquo;</i></a></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- endbuild -->

    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 30,
            loop: true,
            autoplay: {
              delay: 8000,  
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            }
        });

    </script>
</body>

</html>
