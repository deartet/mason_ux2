<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="vault-track-spending">



    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-profile.php"; 
        ?>
        <div class="container fix-little-content-width">
            <div class="row login_first-row">
                <div class="col-md-12">
                    <div class="o_content-box">
                        <section>
                            <h1 class="m_heading1"><span class="icon icon--budget"></span><span class="m_heading1__title">預算設置</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>

                            <div class="horizontal-scrollable-navs ">
                                <div class="navbar-navs" id="navbar-navs">
                                    <div style="margin-left: 15px;" class="navButton" model-id="model1"><a href="#">2017<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model2"><a href="#">1月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model3"><a href="#">2月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model4"><a href="#">3月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model5"><a href="#">4月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model6"><a href="#">5月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model7"><a href="#">6月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model8"><a href="#">7月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model9"><a href="#">8月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model10"><a href="#">9月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model11"><a href="#">10月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model12"><a href="#">11月<span class="caret"></span></a></div>
                                    <div class="navButton" model-id="model13"><a href="#">12月<span class="caret"></span></a></div>
                                </div>
                                <div id="optionGroup" class="optionGroup" style="display:none">
                                    <div id="model1" class="option" style="display:none">
                                        <p>Item1-1</p>
                                        <p>Item1-2</p>
                                    </div>
                                    <div id="model2" class="option" style="display:none">
                                        <p>Item2-1</p>
                                        <p>Item2-2</p>
                                        <p>Item2-3</p>
                                        <p>Item2-4</p>
                                    </div>
                                    <div id="model3" class="option" style="display:none">
                                        <p>Item3-1</p>
                                        <p>Item3-2</p>
                                        <p>Item3-3</p>
                                    </div>
                                    <div id="model4" class="option" style="display:none">
                                        <p>Item4-1</p>
                                        <p>Item4-2</p>
                                    </div>
                                    <div id="model5" class="option" style="display:none">
                                        <p>Item5-1</p>
                                        <p>Item5-2</p>
                                        <p>Item5-3</p>
                                        <p>Item5-4</p>
                                    </div>
                                    <div id="model6" class="option" style="display:none">
                                        <p>Item6-1</p>
                                        <p>Item6-2</p>
                                        <p>Item6-3</p>
                                    </div>
                                    <div id="model7" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model8" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model9" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model10" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model11" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model12" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                    <div id="model13" class="option" style="display:none">
                                        <p>Item7-1</p>
                                        <p>Item7-2</p>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-group accordion-budget" id="accordion" role="tablist" aria-multiselectable="true">
                                <h2 class="m_heading2 budget-total"><span class="division--vertical"></span><span class="m_heading2__title">11月總預算</span><span class="budget-num">$146,000</span></h2>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <a role="button">
                                          <img src="/dev/images/img_category-food.png" alt="" width="20px" height="20px">食品酒水
                                        </a>
                                            <span class="budget-num individual">$2,800</span>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <table class="table table-budget-category--secondary">
                                                <tbody>
                                                    <tr>
                                                        <td width="90px;">早餐</td>
                                                        <td><span class="budget-num individual">$2,83232300</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>午餐</td>
                                                        <td><span class="budget-num individual">$800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>晚餐</td>
                                                        <td><span class="budget-num individual">$12,800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>菸酒茶飲料</td>
                                                        <td><span class="budget-num individual">$90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>水果零食</td>
                                                        <td><span class="budget-num individual">$2,800</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <a class="collapsed" role="button">
                                          <img src="/dev/images/img_category-home.png" alt="" width="20px" height="20px">居家物業
                                        </a>
                                            <span class="budget-num individual">$3,000</span>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                             <table class="table table-budget-category--secondary">
                                                <tbody>
                                                    <tr>
                                                        <td width="90px;">早餐</td>
                                                        <td><span class="budget-num individual">$2,83232300</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>午餐</td>
                                                        <td><span class="budget-num individual">$800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>晚餐</td>
                                                        <td><span class="budget-num individual">$12,800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>菸酒茶飲料</td>
                                                        <td><span class="budget-num individual">$90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>水果零食</td>
                                                        <td><span class="budget-num individual">$2,800</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <a class="collapsed" role="button">
                                              <img src="/dev/images/img_category-traffic.png" alt="" width="20px" height="20px">行車交通
                                            </a>
                                            <span class="budget-num individual">$1,500</span>

                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                           <table class="table table-budget-category--secondary">
                                                <tbody>
                                                    <tr>
                                                        <td width="90px;">早餐</td>
                                                        <td><span class="budget-num individual">$2,83232300</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>午餐</td>
                                                        <td><span class="budget-num individual">$800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>晚餐</td>
                                                        <td><span class="budget-num individual">$12,800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>菸酒茶飲料</td>
                                                        <td><span class="budget-num individual">$90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>水果零食</td>
                                                        <td><span class="budget-num individual">$2,800</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <a class="collapsed" role="button">
                                              <img src="/dev/images/img_category-phone.png" alt="" width="20px" height="20px">交流通訊
                                            </a>
                                            <span class="budget-num individual">$1,500</span>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                           <table class="table table-budget-category--secondary">
                                                <tbody>
                                                    <tr>
                                                        <td width="90px;">早餐</td>
                                                        <td><span class="budget-num individual">$2,83232300</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>午餐</td>
                                                        <td><span class="budget-num individual">$800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>晚餐</td>
                                                        <td><span class="budget-num individual">$12,800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>菸酒茶飲料</td>
                                                        <td><span class="budget-num individual">$90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>水果零食</td>
                                                        <td><span class="budget-num individual">$2,800</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <a class="collapsed" role="button">
                                              <img src="/dev/images/img_category-fun.png" alt="" width="20px" height="20px">休閒娛樂
                                            </a>
                                            <span class="budget-num individual">$1,500</span>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                        <div class="panel-body">
                                           <table class="table table-budget-category--secondary">
                                                <tbody>
                                                    <tr>
                                                        <td width="90px;">早餐</td>
                                                        <td><span class="budget-num individual">$2,83232300</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>午餐</td>
                                                        <td><span class="budget-num individual">$800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>晚餐</td>
                                                        <td><span class="budget-num individual">$12,800</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>菸酒茶飲料</td>
                                                        <td><span class="budget-num individual">$90</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>水果零食</td>
                                                        <td><span class="budget-num individual">$2,800</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </section>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
