$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

// max 18 characters
$(function () {
    var len = 18; // 超過18個字以"..."取代
    $(".JQellipsis-article").each(function (i) {
        if ($(this).text().length > len) {
            $(this).attr("title", $(this).text());
            var text = $(this).text().substring(0, len - 1) + "...";
            $(this).text(text);
        }
    });
});


// add delay between iterations when using animate css

window.setInterval(tada, 5000);

function tada() {
    $('.heading--vip-plan.success').removeClass('animated tada').delay(1000).queue(function (next) {
        $(this).addClass('animated tada');
        next();
    });
}


// horizontal scrollable nav
window.onload = function () {
    $('.navButton').click(function () {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        var optionId = "#" + this.getAttribute('model-id');
        var offsetLeft = this.offsetLeft;
        var display = $(optionId)[0].style.display;
        $('.option').hide();
        $(optionId).css('left', offsetLeft + "px");
        if (display === "none") {
            $(optionId).show();
            $('#optionGroup').show();
        } else {
            $(optionId).hide();
            $('#optionGroup').hide();
        }
    });

    $('#navbar-navs').scroll(function () {
        var scrollLeft = -(this.scrollLeft);
        console.dir(this);
        $('#optionGroup').css('left', scrollLeft + "px");
    });

    $('body').click(function () {
        $('.option').hide();
    });
}

var animated = false;
$('.btn-primary').click(function(){
  if(!animated){
    $(this).addClass('happy');
    animated = true;
  }
  else {
    $(this).removeClass('happy');
    animated = false;
  }
}); 

// 頁面讀取完成會自動跑的js
$(function () {
  $('[data-toggle="popover"]').popover();
})
