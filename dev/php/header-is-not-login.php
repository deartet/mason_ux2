<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
            <div class="m_brand">
                <a class="navbar-brand brand__logo" href="/index.php">
                        <img src="/dev/images/img-logo@2x.png" alt="">
                    </a>
                <span class="brand__mascot">
                            <img src="/dev/images/brand__mascot.png" alt="">
                        </span>
                <div class="brand__titles">
                    <div class="brand__title">錢管家</div>
                    <div class="brand__subtitle">最好的智能理財一站式平台</div>
                </div>
            </div>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav nav-bottom">
                <li class="active"><a href="#"><span class="nav__border">首頁</span></a></li>
                <li><a href="/vault-overview.php"><span class="nav__border">理財</span></a></li>
                <li><a href="#"><span class="nav__border">保險</span></a></li>
                <li><a href="#"><span class="nav__border">投資</span></a></li>
                <li><a href="#"><span class="nav__border">繳費</span></a></li>
                <li><a href="#"><span class="nav__border">Money錢雜誌</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right nav-top">
                <li><a href="#"><span class="icon icon--novice"></span><span class="navbar__top-link">新手導航</span></a></li>
<!--    換成gmail 大頭貼    -->
                <li class="icon__login-container"><a href="/login.php" style="padding-right: 0;"><span class="icon__login"><img src="/dev/images/img_google-avatar.png" class="img-responsive" alt=""></span><span class="navbar__top-link">登入</span></a><i class="navbar__top-link">&#124;</i><a href="/signup.php"><span class="navbar__top-link">註冊</span></a></li>
                <li><a href="#" class="btn btn-vip"> <span class="navbar__top-link">VIP開通</span> <span class="icon icon--signup"></span></a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
