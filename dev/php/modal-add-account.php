<!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-primary pull-right" data-toggle="modal" data-target="#addAccount-cash"><span class="icon-confirm"></span>新增帳戶</button>
<!-- Modal -->
<div class="modal fade slide-from-btm" id="addAccount-cash" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="track-spending--mobile">
                <h1 class="m_heading1"><span class="icon icon--bankbook"></span><span class="m_heading1__title">新增帳戶</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </h1>
                <div class="track-spending_container">
                    <form class="form-horizontal track-spending-input my-3">
                        <div class="form-group">
                            <label for="account-type" class="col-md-2 control-label">帳戶類型</label>
                            <div class="col-md-10">
                                <select class="input-sm form-select">
                                    <option value="1" selected="selected">該tab默認帳戶</option>
                                    <option value="2">資產</option>
                                    <option value="3">儲值卡</option>
                                    <option value="4">負債</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="account-name" class="col-md-2 control-label">帳戶名稱</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="account-name" placeholder="請輸入帳戶名稱">
                            </div>
                        </div>
                        <div class="form-group my-3">
                            <label for="currency" class="col-md-2 control-label">幣值</label>
                            <div class="col-md-10">
                                <select class="input-sm form-select">
                                    <option value="1">新台幣</option>
                                    <option value="2">美元</option>
                                    <option value="3">歐元</option>
                                    <option value="4">人民幣</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="initial-amount" class="col-md-2 control-label">初始金額</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="initial-amount" placeholder="請輸入金額">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="note" class="col-md-2 control-label">備註</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="note" placeholder="請輸入備註">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="button" class="btn btn-track-spending">確認新增</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
