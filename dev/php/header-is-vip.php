<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
            <div class="m_brand">
                <a class="navbar-brand brand__logo" href="/index.php">
                        <img src="/dev/images/img-logo@2x.png" alt="">
                    </a>
                <span class="brand__mascot">
                            <img src="/dev/images/brand__mascot.png" alt="">
                        </span>
                <div class="brand__titles">
                    <div class="brand__title">錢管家</div>
                    <div class="brand__subtitle">最好的智能理財一站式平台</div>
                </div>
            </div>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav nav-bottom">
                <li><a href="/index.php"><span class="nav__border">首頁</span></a></li>
                <li class="active arrow-down"><a href="/index.php"><span class="nav__border">理財</span></a></li>
                <li><a href="#"><span class="nav__border">保險</span></a></li>
                <li><a href="#"><span class="nav__border">投資</span></a></li>
                <li><a href="#"><span class="nav__border">繳費</span></a></li>
                <li><a href="#"><span class="nav__border">Money錢雜誌</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right nav-top">
                <li><a href="#"><span class="icon icon--novice"></span><span class="navbar__top-link">新手導航</span></a></li>
                <li class="icon__login-container dropdown">
                    <!--    change href="http://www.yahoo.com.tw" and data-toggle to # to make a link hyperlinkable -->
                    <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="icon__login"><img src="/dev/images/img_google-avatar.png" class="img-responsive" alt=""></span>
                                <!--    換成gmail 大頭貼    -->

                                <span class="navbar__top-link">rackyrose</span>
                                <span class="caret hidden-xs"></span>
                            </a>
                    <ul class="dropdown-menu hidden-xs" aria-labelledby="dropdownMenu1">
                        <li><a href="/profile-center.php">個人中心</a></li>
                        <li><a href="/profile-common-settings.php">常用設定</a></li>
                        <li><a href="/magazine.php">電子雜誌</a></li>
                    </ul>
                </li>
                <li><a href="#" class="btn btn-vip"><span class="icon icon--vip"></span><span class="navbar__top-link">VIP</span></a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!--
        <section class="nav-scroll">
            <ul class="nav-scroll__list">
                <li>總覽</li>
                <li>帳戶</li>
                <li>報表</li>
                <li>發票</li>
            </ul>
        </section>
-->
<nav class="navbar navbar-default navbar-fixed-top nav-scroll-container">
    <div class="container padding-0">
        <ul class="nav-scroll__list">
            <li><a href="/vault-overview.php" class="active">總覽</a></li>
            <li><a href="/vault-track-spending.php">記帳</a></li>
            <li><a href="/vault-receipt.php">發票</a></li>
            <li><a href="/vault-accounts.php">帳戶</a></li>
            <li><a href="/vault-stats-report.php">報表</a></li>
        </ul>
    </div>
</nav>
