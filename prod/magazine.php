<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
     <link rel="stylesheet" href="css/animate-73305b1ae0.min.css">
     <link rel="stylesheet" href="css/styles-06a9c9d6e5.min.css">
</head>

<body id="">
    <div class="sticky-content">
        <?php include "php/header-is-not-login.php"; ?>

        <!--  step 2  輸入序號 -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">CWMoney VIP 序號兌換<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">請輸入6碼序號</h2>

                            <input type="text" class="form-control mx-auto" placeholder="請輸入6碼序號">

                            <button class="btn btn-coupon">確認</button>
                            <span class="label--corner">開運特刊</span>
                        </div>
                    </div>

                </div>
            </section>
        </div>

        <!--  step 3  輸入序號兌換確認 -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">CWMoney VIP 序號兌換<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">請輸入6碼序號</h2>

                            <input type="text" class="form-control mx-auto" placeholder="請輸入6碼序號">

                            <button class="btn btn-coupon my-5">確認兌換</button>
                            <span class="label--corner">開運特刊</span>
                            <section class="coupon-content_container">
                                <h2 class="m_heading2 text-left"><span class="division--vertical"></span><span class="m_heading2__title">可兌換內容</span></h2>
                                <div class="coupon-content">
                                    <span><span class="icon icon--scissors"></span></span>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">開通方案</div>
                                        <div class="col-sm-10 text-left coupon-content__content">CWMoney VIP</div>
                                    </div>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">方案到期日</div>
                                        <div class="col-sm-10 text-left coupon-content__content"><time>2018 / 2 / 4</time></div>
                                    </div>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">方案內容</div>
                                        <div class="col-sm-10 text-left coupon-content__content">
                                            <ul class="coupon-content__list">
                                                <li><span class="icon icon--circle"></span>手機條碼自動記帳，綁定悠遊卡、一卡通、iCash，一鍵同步消費資訊
                                                </li>
                                                <li><span class="icon icon--circle"></span>掃描發票QR Code自動帶入消費記錄，發票開獎自動對獎
                                                </li>
                                                <li><span class="icon icon--circle"></span>記帳拍照、GPS定位功能，完整紀錄消費內容
                                                </li>
                                                <li><span class="icon icon--circle"></span>資料一鍵上雲端，換手機、換平台、換版本，備份/還原安全不遺失
                                                </li>
                                                <li><span class="icon icon--circle"></span>可傳送記帳紀錄至Gmail、Dropbox，電腦操作也行
                                                </li>
                                                <li><span class="icon icon--circle"></span>動態預算管理功能，一眼看透收支情形
                                                </li>
                                                <li><span class="icon icon--circle"></span>多分類、多帳戶、多幣別，按照個人需求設定
                                                </li>
                                                <li><span class="icon icon--circle"></span>統計報表：圓餅圖、長條圖，清晰呈現收支分佈
                                                </li>
                                                <li><span class="icon icon--circle"></span>月曆檢視每日收支，快速瞭解個人平均消費
                                                </li>
                                                <li><span class="icon icon--circle"></span>桌面Widget快速記帳，快速檢視帳戶情形
                                                </li>
                                                <li><span class="icon icon--circle"></span>多款主題樣式，搭配個人風格
                                                </li>
                                                <li><span class="icon icon--circle"></span>精選理財電子雜誌，隨時掌握金融趨勢</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>

                </div>
            </section>
        </div>

        <!--  step 4  輸入序號兌換成功 -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">CWMoney VIP 序號兌換<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan success animated tada">已兌換成功！</h2>
                            <button class="btn btn-coupon magazine my-5">前往電子雜誌</button>
                            <span class="label--corner">開運特刊</span>
                            <section class="coupon-content_container">
                                <h2 class="m_heading2 text-left"><span class="division--vertical"></span><span class="m_heading2__title">已兌換內容</span></h2>
                                <div class="coupon-content">
                                    <span><span class="icon icon--scissors"></span></span>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">開通方案</div>
                                        <div class="col-sm-10 text-left coupon-content__content">CWMoney VIP</div>
                                    </div>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">方案到期日</div>
                                        <div class="col-sm-10 text-left coupon-content__content"><time>2018 / 2 / 4</time></div>
                                    </div>
                                    <div class="coupon-content__group clearfix">
                                        <div class="col-sm-2 text-right coupon-content__heading">方案內容</div>
                                        <div class="col-sm-10 text-left coupon-content__content">
                                            <ul class="coupon-content__list">
                                                <li><span class="icon icon--circle"></span>手機條碼自動記帳，綁定悠遊卡、一卡通、iCash，一鍵同步消費資訊
                                                </li>
                                                <li><span class="icon icon--circle"></span>掃描發票QR Code自動帶入消費記錄，發票開獎自動對獎
                                                </li>
                                                <li><span class="icon icon--circle"></span>記帳拍照、GPS定位功能，完整紀錄消費內容
                                                </li>
                                                <li><span class="icon icon--circle"></span>資料一鍵上雲端，換手機、換平台、換版本，備份/還原安全不遺失
                                                </li>
                                                <li><span class="icon icon--circle"></span>可傳送記帳紀錄至Gmail、Dropbox，電腦操作也行
                                                </li>
                                                <li><span class="icon icon--circle"></span>動態預算管理功能，一眼看透收支情形
                                                </li>
                                                <li><span class="icon icon--circle"></span>多分類、多帳戶、多幣別，按照個人需求設定
                                                </li>
                                                <li><span class="icon icon--circle"></span>統計報表：圓餅圖、長條圖，清晰呈現收支分佈
                                                </li>
                                                <li><span class="icon icon--circle"></span>月曆檢視每日收支，快速瞭解個人平均消費
                                                </li>
                                                <li><span class="icon icon--circle"></span>桌面Widget快速記帳，快速檢視帳戶情形
                                                </li>
                                                <li><span class="icon icon--circle"></span>多款主題樣式，搭配個人風格
                                                </li>
                                                <li><span class="icon icon--circle"></span>精選理財電子雜誌，隨時掌握金融趨勢</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>

                </div>
            </section>
        </div>


        <!--  step 5  電子雜誌列表 -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">電子雜誌列表<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <section class="o_content-box">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="thumbnail">
                                        <a href="#"><img class="card-img-top" alt="" src="/dev/images/img_magazine-1.png"></a>
                                        <div class="caption">
                                            <h3>Money錢雜誌第122期</h3>
                                            <p><a href="#">2017年10月</a></p>
                                            <span>（精選）</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="thumbnail">
                                        <a href="#"><img class="card-img-top" alt="" src="/dev/images/img_magazine-2.png"></a>
                                        <div class="caption">
                                            <h3>Money錢雜誌第121期</h3>
                                            <p><a href="#">2017年9月</a></p>
                                            <span>（精選）</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="thumbnail">
                                        <a href="#"><img class="card-img-top" alt="" src="/dev/images/img_magazine-3.png"></a>
                                        <div class="caption">
                                            <h3>Money錢雜誌第120期</h3>
                                            <p><a href="#">2017年8月</a></p>
                                            <span>（精選）</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>


            </section>






        </div>
    </div>
    <!-- /. sticky-content -->
    <?php include "php/footer.php"; ?>


    <script src="js/jquery-cdc2ba15b7.min.js"></script>
    <script src="js/bootstrap-22621c24c0.min.js"></script>

    <script src="js/myscript-7f42199f3b.min.js"></script>
</body>

</html>
