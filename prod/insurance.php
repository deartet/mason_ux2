<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
     <link rel="stylesheet" href="css/styles-06a9c9d6e5.min.css">

</head>

<body id="money-mgmt">
    <div class="sticky-content">
        <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
            <div class="m_brand">
                <a class="navbar-brand brand__logo" href="#">
                        <img src="/dev/images/img-logo@2x.png" alt="">
                    </a>
                <span class="brand__mascot">
                            <img src="/dev/images/brand__mascot.png" alt="">
                        </span>
                <div class="brand__titles">
                    <div class="brand__title">錢管家</div>
                    <div class="brand__subtitle">最好的智能理財一站式平台</div>
                </div>
            </div>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav nav-bottom">
                <li ><a href="#"><span class="nav__border">首頁</span></a></li>
                <li class="active arrow-down"><a href="#"><span class="nav__border">理財</span></a></li>
                <li ><a href="#"><span class="nav__border">保險</span></a></li>
                <li ><a href="#"><span class="nav__border">投資</span></a></li>
                <li ><a href="#"><span class="nav__border">繳費</span></a></li>
                <li ><a href="#"><span class="nav__border">Money錢雜誌</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right nav-top">
                <li><a href="#"><span class="icon icon--novice"></span><span class="navbar__top-link">新手導航</span></a></li>
                <li class="icon__login-container"><a href="#"><span class="icon__login"></span><span class="navbar__top-link">登入</span></a><i class="navbar__top-link">&#124;</i><a href="#"><span class="navbar__top-link">註冊</span></a></li>
                <li><a href="#" class="btn btn-vip"> <span class="navbar__top-link">VIP開通</span> <span class="icon icon--signup"></span></a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

        <!--
        <section class="nav-scroll">
            <ul class="nav-scroll__list">
                <li>總覽</li>
                <li>帳戶</li>
                <li>報表</li>
                <li>發票</li>
            </ul>
        </section>
-->
        <nav class="navbar navbar-default navbar-fixed-top nav-scroll-container">
            <div class="container">
                <ul class="nav-scroll__list">
                    <li><a href="#" class="active">總覽</a></li>
                    <li><a href="#">帳戶</a></li>
                    <li><a href="#">報表</a></li>
                    <li><a href="#">發票</a></li>
                </ul>
            </div>
        </nav>
        
        
        <div class="container">
            <div class="o_content-box home_full-height-content-box home_section-money-mgmt" style="margin-top: 300px;">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>理財</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-5 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">理財新知</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                                <div class="media-container media--full-height">
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">跟朋友要錢就靠 「它」出國記帳才能理直氣壯『算帳』！</h2>
                                            </a>
                                            <p class="media__browse-times">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">3500 萬發票獎金竟然沒有我的份？全是因為你 沒看「完全攻略」
                                                </h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-6.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">「帳單沒繳」、「懶得記帳」！3 個理財壞習慣，搞亂你的腦袋？</h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <section class="home-moneymgmt-services-box-container">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--feature-services"></span><span class="m_heading2__title">三大理財服務</span></h2>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_track-spending.svg" alt="">
                                                        <h3>記帳</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">理財的第一步</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_paybill.svg" alt="">
                                                        <h3>繳費</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">隨繳隨記</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_receipt-lottery.svg" alt="">
                                                        <h3>發票</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">掃發票對獎</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <?php include "php/snippet-btn-get-started.php"; ?>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container">
            <div class="o_content-box home_full-height-content-box home_section-money-mgmt">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>理財</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-5 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">理財新知</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                                <div class="media-container media--full-height">
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">跟朋友要錢就靠 「它」出國記帳才能理直氣壯『算帳』！</h2>
                                            </a>
                                            <p class="media__browse-times">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">3500 萬發票獎金竟然沒有我的份？全是因為你 沒看「完全攻略」
                                                </h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-6.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">「帳單沒繳」、「懶得記帳」！3 個理財壞習慣，搞亂你的腦袋？</h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <section class="home-moneymgmt-services-box-container">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--feature-services"></span><span class="m_heading2__title">三大理財服務</span></h2>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_track-spending.svg" alt="">
                                                        <h3>記帳</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">理財的第一步</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_paybill.svg" alt="">
                                                        <h3>繳費</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">隨繳隨記</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_receipt-lottery.svg" alt="">
                                                        <h3>發票</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">掃發票對獎</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <?php include "php/snippet-btn-get-started.php"; ?>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container">
            <div class="o_content-box home_full-height-content-box home_section-money-mgmt">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>理財</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-5 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">理財新知</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                                <div class="media-container media--full-height">
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">跟朋友要錢就靠 「它」出國記帳才能理直氣壯『算帳』！</h2>
                                            </a>
                                            <p class="media__browse-times">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">3500 萬發票獎金竟然沒有我的份？全是因為你 沒看「完全攻略」
                                                </h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-6.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">「帳單沒繳」、「懶得記帳」！3 個理財壞習慣，搞亂你的腦袋？</h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm">瀏覽人次：37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <section class="home-moneymgmt-services-box-container">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--feature-services"></span><span class="m_heading2__title">三大理財服務</span></h2>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_track-spending.svg" alt="">
                                                        <h3>記帳</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">理財的第一步</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_paybill.svg" alt="">
                                                        <h3>繳費</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">隨繳隨記</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_receipt-lottery.svg" alt="">
                                                        <h3>發票</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">掃發票對獎</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <?php include "php/snippet-btn-get-started.php"; ?>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /. sticky-content -->

    <?php include "php/footer.php"; ?>

  
    <script src="js/jquery-cdc2ba15b7.min.js"></script>
    <script src="js/bootstrap-22621c24c0.min.js"></script>

    <script src="js/myscript-7f42199f3b.min.js"></script>
</body>

</html>
