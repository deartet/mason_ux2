<footer>
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">

                            <a class="footer_brand" href="#">
                                <img class="img-responsive" src="/images/img-logo@2x.png" alt="">
                            </a>

                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <h3 class="footer-heading">
                                關於錢管家
                            </h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="footer__links">
                                        <li><a href="#">關於我們</a></li>
                                        <li><a href="#">聯絡我們</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <h3 class="footer-heading">
                                條款聲明
                            </h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="footer__links">
                                        <li><a href="#">服務條款</a></li>
                                        <li><a href="#">隱私權聲明</a></li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                        <div class="col-sm-3">
                            <h3 class="footer-heading">
                                產品與服務
                            </h3>
                            <div class="row">
                                <div class="col-xs-6">
                                    <ul class="footer__links left">
                                        <li><a href="#">記帳</a></li>
                                        <li><a href="#">發票</a></li>
                                        <li><a href="#">繳費</a></li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="footer__links right">
                                        <li><a href="#">保險</a></li>
                                        <li><a href="#">投資</a></li>
                                        <li><a href="#">雜誌</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-btm text-center">
                Copyright @ 2017 金尉股份有限公司 All rights reserved
            </div>
        </footer>