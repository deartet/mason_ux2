<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <link rel="stylesheet" href="css/styles-06a9c9d6e5.min.css">

</head>

<body id="vault">
    <div class="sticky-content">
        <?php 
//            include "php/header-is-not-vip.php";  
              include "php/header-is-vip.php"; 
        ?>
        <div class="container">
            <div class="row login_first-row">
                <div class="col-sm-8">
                    <section class="o_content-box vault-profile">
                        <div class="vault-profile__banner vault-profile--top">
                            <blockquote>
                                <p>“ 錯誤並不可恥，可恥的是錯誤已經顯而易見了卻還不去修正! ”</p>
                                <footer>索羅斯<cite title="Source Title"></cite></footer>
                            </blockquote>
                        </div>
                        <div class="vault-profile--bottom">
                            <div class="vault-profile__photo">
                                <a href="#"><img src="/dev/images/img_profile.png" alt=""></a>
                            </div>
                            <div class="vault-profile__name">Martina Hingis</div>
                            <div class="vault-profile__stats">
                                <a href="#">
                                    30
                                </a>
                                <a href="#">
                                    2310
                                </a>
                            </div>
                        </div>
                    </section>
                    <section class="o_content-box">
                        <div class="panel panel-default">
     <div class="panel-heading">Title</div>
     <div class="panel-body">
        <div class="row">
            <div class="col-md-6">col1</div><div class="col-md-6">col2</div>
        </div> 
        <div class="row">
            <div class="col-md-6">col1</div><div class="col-md-6">col2</div>
        </div> 
        <div class="row">
            <div class="col-md-6">col1</div><div class="col-md-6">col2</div>
        </div> 
     </div>
</div>
                    </section>
                </div>
                <div class="col-sm-4">
                    <div class="o_content-box">
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "php/footer.php"; ?>


    <script src="js/jquery-cdc2ba15b7.min.js"></script>
    <script src="js/bootstrap-22621c24c0.min.js"></script>

    <script src="js/myscript-7f42199f3b.min.js"></script>
</body>

</html>
