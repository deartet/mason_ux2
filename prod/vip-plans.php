<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
      <link rel="stylesheet" href="css/animate-73305b1ae0.min.css">
    <link rel="stylesheet" href="css/styles-06a9c9d6e5.min.css">

</head>

<body id="">
    <div class="sticky-content">
        <?php include "php/header-is-not-login.php"; ?>

        <!--    step 1 VIP方案選擇       -->
        <div class="container">
            <section class="my-vip">
                <h1 class="heading--vip">方案<span class="underline--short"></span></h1>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">CWMoney VIP 序號兌換</h2>
                            <button class="btn btn-coupon">點擊兌換</button>
                            <span class="label--corner"> <span class="animated tada">開運特刊</span></span>
                        </div>
                    </div>
                    <!--      for future vip plans              -->
                    <div class="col-sm-6 hidden">
                        <div class="o_content-box vip">
                            <h2 class="heading--vip-plan">CWMoney VIP 序號兌換</h2>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /. sticky-content -->
    <?php include "php/footer.php"; ?>

    
    <script src="js/jquery-cdc2ba15b7.min.js"></script>
    <script src="js/bootstrap-22621c24c0.min.js"></script>

    <script src="js/myscript-7f42199f3b.min.js"></script>
</body>

</html>
