<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
    <link rel="stylesheet" href="css/styles-06a9c9d6e5.min.css">

</head>

<body id="signup">
    <div class="sticky-content">
        <?php include "php/header.php"; ?>
        
        <form class="form-horizontal l-signup signup">
            <div class="login_heading-container my-5">
                <div class="login_heading">重新取得密碼</div>
            </div>
            
            <div class="form-group has-feedback">
                <div class="col-sm-12">
                    <div class="input-group my-3">
                        <span class="input-group-addon"><span class="icon icon--email"></span></span>
                        <input type="text" class="form-control" id="inputGroupSuccess2" aria-describedby="inputGroupSuccess2Status" placeholder="請輸入email地址">
                        <!--
                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                    <span id="inputGroupSuccess2Status" class="sr-only">(success)</span>
-->
                    </div>
                </div>

            </div>
        
            

            <div class="form-group my-3">
                <button type="submit" class="btn btn-default btn-block btn-lg btn-reset-pwd">重置密碼</button>
            </div>
          


            <div class="form-group">
                <div class="founder">Powered by Money.com.tw</div>
            </div>
        </form>
        
    </div>
    <!-- /. sticky-content -->
    <?php include "php/footer.php"; ?>

 
    <script src="js/jquery-cdc2ba15b7.min.js"></script>
    <script src="js/bootstrap-22621c24c0.min.js"></script>

    <script src="js/myscript-7f42199f3b.min.js"></script>
</body>

</html>
