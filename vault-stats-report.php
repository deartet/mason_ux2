<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->
    <style>
        /*
        .hidden-first-row,
        .hidden-second-row {
            display: none;
        }

        .shown {
            display: table-row !important;
        }
*/

    </style>



</head>

<body id="vault-accounts">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container fix-little-content-width">
            <div class="row login_first-row">
                <div class="col-md-12">
                    <div class="o_content-box">
                        <section>
                            <header class="account-detail">
                                <h1 class="m_heading1"><span class="icon icon--graph"></span><span class="m_heading1__title">收支報表</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                            </header>
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-primary" role="tablist" style="margin-top: 35px;">
                                <li role="presentation" class="active"><a href="#in-out-daily" aria-controls="spend" role="tab" data-toggle="tab">日常收支表</a></li>
                                <li role="presentation"><a href="#in-out-trends" aria-controls="income" role="tab" data-toggle="tab">收支趨勢圖</a></li>
                                <li role="presentation"><a href="#in-out-ratios" aria-controls="transfer" role="tab" data-toggle="tab">收支比例圖</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="in-out-daily">
                                    <div class="horizontal-scrollable-navs ">
                                        <div class="navbar-navs" id="navbar-navs">

                                            <div class="navButton" model-id="model1"><a href="#"><span class="icon icon--time"></span>全部時間<span class="caret"></span></a></div>
                                            <div class="navButton" model-id="model2"><a href="#"><span class="icon icon--category"></span>全部分類<span class="caret"></span></a></div>
                                            <div class="navButton" model-id="model3"><a href="#"><span class="icon icon--bankbook-report"></span>全部帳戶<span class="caret"></span></a></div>
                                            <div class="navButton" model-id="model4"><a href="#"><span class="icon icon--tag"></span>全部標籤<span class="caret"></span></a></div>
                                            <div style="display: inline-block; display: none;">
                                                <div class="input-groups">
                                                    <input type="text" placeholder="搜尋備註">
                                                    <button type="button" class="btn-primary">搜尋</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="optionGroup" class="optionGroup" style="display:none">
                                            <div id="model1" class="option" style="display:none">
                                                <p>Item1-1</p>
                                                <p>Item1-2</p>
                                            </div>
                                            <div id="model2" class="option" style="display:none">
                                                <p>Item2-1</p>
                                                <p>Item2-2</p>
                                                <p>Item2-3</p>
                                                <p>Item2-4</p>
                                            </div>
                                            <div id="model3" class="option" style="display:none">
                                                <p>Item3-1</p>
                                                <p>Item3-2</p>
                                                <p>Item3-3</p>
                                            </div>
                                            <div id="model4" class="option" style="display:none">
                                                <p>Item4-1</p>
                                                <p>Item4-2</p>
                                            </div>
                                            <div id="model5" class="option" style="display:none">
                                                <p>Item5-1</p>
                                                <p>Item5-2</p>
                                                <p>Item5-3</p>
                                                <p>Item5-4</p>
                                            </div>
                                            <div id="model6" class="option" style="display:none">
                                                <p>Item6-1</p>
                                                <p>Item6-2</p>
                                                <p>Item6-3</p>
                                            </div>
                                            <div id="model7" class="option" style="display:none">
                                                <p>Item7-1</p>
                                                <p>Item7-2</p>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="filter-conditions">
                                        <li>食品酒水
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>2017/11/1 ~ 2017/11/12
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>信用卡帳戶
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>出差台北
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>行車交通
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                    </ul>
                                    <img src="/dev/images/img_stats-graph-1.png" class="img-responsive" style="margin: auto;" alt="">
                                    <div class="">
                                        <table class="table table-striped account-detail-table" style="max-width: 800px; margin: auto; margin-bottom: 45px;">
                                            <thead>
                                                <tr>
                                                    <th><a href="#" class="table-filter">分類</a></th>
                                                    <th style="text-align: center;"><a href="#" class="table-filter">金額</a></th>
                                                    <th style="text-align: center;"><a href="#" class="table-filter">支出/總收入(%)</a></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="show-dropdown-row">
                                                    <td>食品酒水</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$500</div>
                                                    </td>

                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>
                                                </tr>
                                                <tr class="hidden-first-row">
                                                    <td>早餐</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5300</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>3%</div>
                                                    </td>
                                                </tr>
                                                <tr class="hidden-second-row">
                                                    <td>三明治</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$32500</div>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr class="hidden-third-row">
                                                    <td>漢堡</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50320</div>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td>居家物業</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$500</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>30%</div>
                                                    </td>


                                                </tr>
                                                <tr>
                                                    <td>行車交通</td>

                                                    <td class="table-num-align-center-right">
                                                        <div>$21500</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>103%</div>
                                                    </td>


                                                </tr>
                                                <tr>
                                                    <td>交流通訊</td>

                                                    <td class="table-num-align-center-right">
                                                        <div>$188</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>


                                                </tr>
                                                <tr>
                                                    <td>休閒娛樂</td>

                                                    <td class="table-num-align-center-right">
                                                        <div>$5300</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>1320%</div>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td>進修學習</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$530</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>103%</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>人情往來</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5030</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>30%</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>醫療保健</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5010</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>130%</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>金融保險</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>其他雜項</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$10</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>電子發票帶入</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                    <img src="/dev/images/img_stats-graph-2.png" class="img-responsive" style="margin: auto;" alt="">
                                    <div class="">
                                        <table class="table table-striped account-detail-table" style="max-width: 800px; margin: auto;">
                                            <thead>
                                                <tr>
                                                    <th>分類</th>

                                                    <th style="text-align:center;"><a href="#" class="table-filter">金額</a></th>
                                                    <th style="text-align:center;"><a href="#" class="table-filter">收入/總收入(%)</a></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>工作收入</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>10%</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>其他收入</td>

                                                    <td class="table-num-align-center-right">
                                                        <div>$50320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>130%</div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="in-out-trends">
                                    <div class="horizontal-scrollable-navs">
                                        <div class="navbar-navs" id="navbar-navs">
                                            <div class="checkbox-group">
                                                <div class="checkbox--filter">
                                                    <input type="checkbox" id="checkbox-income" name="">
                                                    <label for="checkbox-income">收入</label>
                                                </div>
                                                <div class="checkbox--filter">
                                                    <input type="checkbox" id="checkbox-spend" name="">
                                                    <label for="checkbox-spend">支出</label>
                                                </div>
                                                <div class="checkbox--filter">
                                                    <input type="checkbox" id="checkbox-remaining" name="">
                                                    <label for="checkbox-remaining">餘額</label>
                                                </div>
                                            </div>
                                            <div class="navButton" model-id="model5"><a href="#"><span class="icon icon--time"></span>全部時間<span class="caret"></span></a></div>
                                            <div class="navButton" model-id="model6"><a href="#"><span class="icon icon--category"></span>全部分類<span class="caret"></span></a></div>

                                        </div>
                                        <div id="optionGroup" class="optionGroup" style="display:none">
                                            <div id="model5" class="option" style="display:none">
                                                <p>Item1-1</p>
                                                <p>Item1-2</p>
                                            </div>
                                            <div id="model6" class="option" style="display:none">
                                                <p>Item2-1</p>
                                                <p>Item2-2</p>
                                                <p>Item2-3</p>
                                                <p>Item2-4</p>
                                            </div>


                                        </div>
                                    </div>


                                    <ul class="filter-conditions">
                                        <li>2017/11/1 ~ 2017/11/12
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>支出
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                    </ul>
                                    <img src="/dev/images/img_stats-graph-3.png" class="img-responsive" style="margin: auto;" alt="">
                                    <div class="">
                                        <table class="table table-striped account-detail-table" style="max-width: 800px; margin: auto; margin-bottom: 45px;">
                                            <thead>
                                                <tr>
                                                    <th><a href="#" class="table-filter">時間</a></th>
                                                    <th style="text-align: center;"><a href="#" class="table-filter">收入</a></th>
                                                    <th style="text-align: center;"><a href="#" class="table-filter">支出</a></th>
                                                    <th style="text-align: center;"><a href="#" class="table-filter">餘額</a></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr style="background-color: #FFFEDA;">
                                                    <td style="font-weight: 900;">2017(總)</td>
                                                    <td class="table-num-align-center-right">
                                                        <div style="color: #61B329; font-weight: 900;">$1,150,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div style="color: #FF4500; font-weight: 900;">$50,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div style="font-weight: 900; color: #212121;">$1,100,000</div>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>2017/10</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,100,000</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2017/09</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,120</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$520</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,100,000</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2017/08</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,150,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,100,000</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2017/07</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,150,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1,100,000</div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="in-out-ratios">
                                    <div class="horizontal-scrollable-navs ">
                                        <div class="navbar-navs" id="navbar-navs">
                                            <div class="navButton" model-id="model9" style="margin-left: 15px;"><a href="#"><span class="icon icon--time"></span>全部時間<span class="caret"></span></a></div>
                                            <div class="navButton" model-id="model9"><a href="#"><span class="icon icon--category"></span>全部分類<span class="caret"></span></a></div>
                                        </div>
                                        <div id="optionGroup" class="optionGroup" style="display:none">
                                            <div id="model10" class="option" style="display:none">
                                                <p>Item1-1</p>
                                                <p>Item1-2</p>
                                            </div>
                                            <div id="model10" class="option" style="display:none">
                                                <p>Item2-1</p>
                                                <p>Item2-2</p>
                                                <p>Item2-3</p>
                                                <p>Item2-4</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="filter-conditions">
                                        <li>食品酒水
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>2017/11/1 ~ 2017/11/12
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                        <li>行車交通
                                            <button type="button" class="close" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="/dev/images/img_stats-graph-4.png" class="img-responsive" style="margin: auto;" alt="">
                                            <div class="">
                                                <table class="table table-striped account-detail-table" style="max-width: 800px; margin: auto; margin-bottom: 45px;">
                                                    <thead>
                                                        <tr>
                                                            <th><a href="#" class="table-filter">分類</a></th>
                                                            <th style="text-align:center;"><a href="#" class="table-filter">金額</a></th>
                                                            <th style="text-align:center;"><a href="#" class="table-filter">支出/總收入(%)</a></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>食品酒水</td>
                                                              <td class="table-num-align-center-right">
                                                        <div>$2500</div>
                                                    </td>
                                                             <td class="table-num-align-center-right">
                                                        <div>16.67%</div>
                                                    </td>
                                                        </tr>
                                                        <tr>
                                                            <td>食品酒水</td>
                                                             <td class="table-num-align-center-right">
                                                        <div>$500</div>
                                                    </td>
                                                            <td class="table-num-align-center-right">
                                                        <div>16.67%</div>
                                                    </td>
                                                        </tr>
                                                        <tr>
                                                            <td>食品酒水</td>
                                                            <td class="table-num-align-center-right">
                                                        <div>$320</div>
                                                    </td>
                                                            <td class="table-num-align-center-right">
                                                        <div>7%</div>
                                                    </td>
                                                        </tr>
                                                        <tr>
                                                            <td>食品酒水</td>
                                                           <td class="table-num-align-center-right">
                                                        <div>$200</div>
                                                    </td>
                                                           <td class="table-num-align-center-right">
                                                        <div>7%</div>
                                                    </td>
                                                        </tr>
                                                        <tr>
                                                            <td>食品酒水</td>
                                                            <td class="table-num-align-center-right">
                                                        <div>$300</div>
                                                    </td>
                                                            <td class="table-num-align-center-right">
                                                        <div>70%</div>
                                                    </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="/dev/images/img_stats-graph-5.png" class="img-responsive" style="margin: auto;" alt="">
                                            <div class="">
                                                <table class="table table-striped account-detail-table" style="max-width: 800px; margin: auto; margin-bottom: 45px;">
                                                    <thead>
                                                        <tr>
                                                           <th><a href="#" class="table-filter">分類</a></th>
                                                            <th style="text-align:center;"><a href="#" class="table-filter">金額</a></th>
                                                            <th style="text-align:center;"><a href="#" class="table-filter">支出/總收入(%)</a></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>職業收入</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$7,300</div>
                                                            </td>
                                                            <td class="table-num-align-center-right">
                                                                <div>58.83%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>職業收入</td>
                                                             <td class="table-num-align-center-right">
                                                                <div>$700</div>
                                                            </td>
                                                            <td class="table-num-align-center-right">
                                                                <div>58.83%</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>職業收入</td>
                                                             <td class="table-num-align-center-right">
                                                                <div>$700</div>
                                                            </td>
                                                            <td class="table-num-align-center-right">
                                                                <div>58.83%</div>
                                                            </td>


                                                        </tr>
                                                        <tr>
                                                            <td>職業收入</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$132,300</div>
                                                            </td>
                                                            <td class="table-num-align-center-right">
                                                                <div>58.83%</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>職業收入</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$500</div>
                                                            </td>
                                                            <td class="table-num-align-center-right">
                                                                <div>58.83%</div>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-circle hidden-md" data-toggle="modal" data-target="#myModal">
                  
                    </button>

                        <!-- Modal -->
                        <div class="modal fade slide-from-btm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="track-spending--mobile">
                                        <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">快速記帳</span>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </h1>

                                        <div class="track-spending_container">
                                            <form class="form-horizontal track-spending-input my-3">
                                                <div class="form-group">
                                                    <label for="spend-date" class="col-md-2 control-label">日期</label>
                                                    <div class="col-md-10">
                                                        <input type="text" style="min-width:96%" class="form-control" id="spend-date" placeholder="請輸入日期">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="spend-money" class="col-md-2 control-label">金額</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" id="spend-money" placeholder="請輸入金額">
                                                    </div>
                                                </div>
                                                <div class="form-group my-3">
                                                    <label for="spend-category" class="col-md-2 control-label">分類</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" id="spend-category" placeholder="請輸入分類">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="button" class="btn btn-track-spending">確認記帳</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <table class="table track-spending-table">
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-food.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">菸酒茶飲料 / $60</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-fun.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">運動健身 / $1600</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-traffic.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">行動交通 / $70</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <td colspan="2"><a href="#" class="content-more">更多 <i>&raquo;</i></a></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .rr-show {
                            display: block !important;
                        }

                        .rr-hidden {
                            display: none;
                        }

                    </style>
                    <script>
                        var str = '<form class="form-inline track-spending-form inside"> ';
                        str += '                        <div class="form-group track-spending-input">';
                        str += '                            <label for="amount" class="control-label">帳戶</label>';
                        str += '                           <input type="text" class="form-control" id="amount" placeholder="請輸入帳戶">';
                        str += '                        </div>';
                        str += '                       <div class="form-group track-spending-input">';
                        str += '                         <label for="account" class="control-label">幣值</label>';
                        str += '                            <input type="text" class="form-control" id="account" placeholder="請輸入幣值">';
                        str += '                        </div>';
                        str += '                        <div class="form-group track-spending-input">';
                        str += '                            <label for="receipt" class="control-label">帳戶金額</label>';
                        str += '                            <input type="text" class="form-control" id="receipt" placeholder="請輸入帳戶金額">';
                        str += '                       </div>';
                        str += '                        <div class="form-group track-spending-input">';
                        str += '                            <label for="category" class="control-label">初始金額</label>';
                        str += '                           <input type="text" class="form-control" id="category" placeholder="請輸入初始金額">';
                        str += '                       </div>';
                        str += '                        <div class="form-group track-spending-input">';
                        str += '                            <label for="tag" class="control-label">備註</label>';
                        str += '                       <input type="text" class="form-control" id="tag" placeholder="請輸入備註">';
                        str += '                       </div>';
                        str += '                       <div class="form-group" style="display: block;">';
                        str += '                         <button type="button" class="btn btn-default">重整</button>';
                        str += '                            <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;">確認</button>';
                        str += '                        </div>';
                        str += '                         <div class="form-group" style="display: block;margin-top: 10px;">';
                        str += '                            <button type="button" class="btn btn-default btn-delete">刪除</button>';
                        str += '                        </div>';
                        str += '           </form>';
                        //                     
                        var last = '';

                        function show(who) {


                            last = who;
                            $('#' + who).slideUp('slow');
                            $('#' + who).html(str);
                            $('#' + who).attr('class', 'rr-panel rr-show');
                            /*
                                                $('#'+target_last).promise().done(function(){

                                                    $('#'+target_div).html(billAry[category]);
                                                    // $('#'+target_another_div).html('');
                                                    $('#'+target_div).attr('class','pay-panel-list pay-panel-'+category);

                                                    $('#'+target_div).slideDown('slow');

                                                });
                            */
                        }

                    </script>

                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
