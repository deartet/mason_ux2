<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <!-- endbuild -->
    <style>
        .fc-event {
            background-color: #fa9126 !important;
            border: none !important;
            border-radius: 2px !important;
            padding: 1px !important;
        }

        #calendar a {
            /*            color: #5c8cd8;*/
        }

        .fc-toolbar h2 {
            color: #212121;
            font-size: 20px;
        }

        .fc-unthemed td.fc-today {
            background-color: hsl(30, 95%, 94%) !important;
        }

        @media screen and (max-width: 580px) {
            .fc-center {
                margin-top: 15px;
            }
        }

        @media screen and (max-width: 413px) {
            .fc-right {
                margin-top: 20px;
            }
        }

    </style>
</head>

<body id="vault">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container">
            <div class="row login_first-row">
                <div class="col-md-8">
                    <section class="o_content-box vault-profile">
                        <div class="vault-profile__banner vault-profile--top">
                            <blockquote>
                                <p>“ 我始終知道我會富有。對此我不曾有過一絲一刻的懷疑。”</p>
                                <footer>巴菲特<cite title="Source Title"></cite></footer>
                            </blockquote>
                        </div>
                        <div class="vault-profile--bottom">
                            <div class="vault-profile__photo">
                                <a href="#"><img src="/dev/images/img_profile.png" alt=""></a>
                            </div>
                            <div class="vault-profile__name">Martina Hingis</div>
                            <div class="vault-profile__stats hidden-xxs">
                                <a href="#">
                                    30
                                </a>
                                <a href="#">
                                    2310
                                </a>
                            </div>
                        </div>
                    </section>
                    <section class="o_content-box total-asset">
                        <h1 class="m_heading1"><span class="icon icon--total-asset"></span><span class="m_heading1__title">資產總覽</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a><a href="#" class="total-asset-lock" data-toggle="modal" data-target=".bs-example-modal-sm"></a></h1>
                        <!--              modal for private lock asset     -->
                        <div class="modal modal--lock-asset fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputPwd" class="col-sm-3 control-label"><span class="icon icon--key"></span>解鎖</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="inputPwd" placeholder="請輸入密碼">
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-bottom: 0;">
                                            <div class="col-sm-offset-6 col-sm-10">
                                                <button type="submit" class="btn btn-default btn-primary">確認</button>
                                                <button type="submit" class="btn btn-default">取消</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="total-asset-general">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">資產概況</span><a href="#" class="total-asset__content-more"></a><a href="#" class="content-more">管理 <i>&raquo;</i></a></h2>
                            <div class="Rtable Rtable--3cols Rtable--collapse">
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>總資產</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell total-asset__asset">NT$ 3,000,000</div>
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>總負債</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell total-asset__liability">NT$ 1,000,000</div>
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>淨資產</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell total-asset__net-value">NT$ 2,000,000</div>
                            </div>
                            <div class="row mx-0">
                                <div class="col-sm-6">
                                    <table class="table table--total-asset">
                                        <thead>
                                            <tr>
                                                <th colspan="2">總資產</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>現金</td>
                                                <td>$85,000</td>
                                            </tr>
                                            <tr>
                                                <td>銀行</td>
                                                <td>$183,000</td>
                                            </tr>
                                            <tr>
                                                <td>保險</td>
                                                <td>$66,000</td>
                                            </tr>
                                            <tr>
                                                <td>股票</td>
                                                <td>$1,743,000</td>
                                            </tr>
                                            <tr>
                                                <td>基金</td>
                                                <td>$923,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <table class="table table--total-asset">
                                        <thead>
                                            <tr>
                                                <th colspan="2">總負債</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>信用卡</td>
                                                <td>$480,000</td>
                                            </tr>
                                            <tr>
                                                <td>銀行</td>
                                                <td>$520,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="total-asset-budget">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">2017/10 預算</span><a href="#" class="content-more">管理 <i>&raquo;</i></a></h2>
                            <div class="Rtable Rtable--3cols Rtable--collapse">
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>本月所有類別預算總額度</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell">$21,000</div>
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>已使用預算</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell">$1,000</div>
                                <div style="order:0;" class="Rtable-cell Rtable-cell--head">
                                    <h3>預算總額度尚餘</h3>
                                </div>
                                <div style="order:1;" class="Rtable-cell">
                                    <div class="progress progress-bar_budget-remain">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            <span class="sr-only">60% Complete</span>
                                        </div>
                                    </div><span>$20,000</span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="total-asset-balance_container my-3">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">2017/10 收支累計</span><a href="#" class="content-more">管理 <i>&raquo;</i></a></h2>
                                    <section class="total-asset-balance-heading_container">
                                        <div class="total-asset-balance-heading left">
                                            支出
                                        </div>
                                        <div class="total-asset-balance-heading placeholder"></div>
                                        <div class="total-asset-balance-heading right">
                                            收入
                                        </div>
                                    </section>
                                    <section class="total-asset-balance">
                                        <div class="total-asset-balance__bar left">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="1000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 10%">
                                                    <span>$1000</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="total-asset-balance__span">本日</div>
                                        <div class="total-asset-balance__bar right">
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="5000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 10%;">
                                                    $5000
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="total-asset-balance">
                                        <div class="total-asset-balance__bar left">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="1000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 60%">
                                                    $6000
                                                </div>
                                            </div>
                                        </div>
                                        <div class="total-asset-balance__span">本週</div>
                                        <div class="total-asset-balance__bar right">
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="5000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 60%;">
                                                    $25,000
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="total-asset-balance">
                                        <div class="total-asset-balance__bar left">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="1000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 70%">
                                                    $24,000
                                                </div>
                                            </div>
                                        </div>
                                        <div class="total-asset-balance__span">本月</div>
                                        <div class="total-asset-balance__bar right">
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="5000" aria-valuemin="0" aria-valuemax="100" style="min-width: 4em; width: 90%;">
                                                    $100,000
                                                </div>
                                            </div>
                                        </div>
                                    </section>



                                    <!--
                                    <div class="total-asset-balance text-center">
                                        <section class="total-asset-balance__bar">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                                    <span class="sr-only">30% Complete</span>
                                                </div>
                                            </div>
                                            <span class="span">本日</span>
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                                    <span class="sr-only">30% Complete</span>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="total-asset-balance__bar second">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                    <span class="sr-only">60% Complete</span>
                                                </div>
                                            </div>
                                            <span class="span">本月</span>
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                    <span class="sr-only">60% Complete</span>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="total-asset-balance__bar third">
                                            <div class="progress progress-bar--left">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                                    <span class="sr-only">90% Complete</span>
                                                </div>
                                            </div>
                                            <span class="span">本月</span>
                                            <div class="progress progress-bar--right">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                                    <span class="sr-only">90% Complete</span>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
-->
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="total-asset-spend-catetory_container">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">2017/10 分類支出</span><a href="#" class="content-more">管理 <i>&raquo;</i></a></h2>
                                    <section class="total-asset-spend-catetory text-center">
                                        <img src="/dev/images/img_placeholder-spend-category.png" alt="">
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="total-asset-monthview_container">
                            <h2 class="m_heading2"><span class="division--vertical"></span><span class="m_heading2__title">收支月檢視</span><a href="#" class="content-more">管理 <i>&raquo;</i></a></h2>
                            <section class="total-asset-monthview">
                                <!--         參考moneyforward.com  網址：https://moneyforward.com/cf   https://fullcalendar.io/                -->
                                <div id="calendar">

                                </div>

                            </section>
                        </div>


                    </section>
                </div>
                <div class="col-md-4">
                    <div class="o_content-box hidden-xs">
                        <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">快速記帳</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                        <div class="track-spending_container">
                            <form class="form-horizontal fast-track-spending-input my-3">
                                <div class="form-group">
                                    <label for="spend-date" class="col-md-2 control-label">日期</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="spend-date" placeholder="請輸入日期">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="spend-money" class="col-md-2 control-label">金額</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="spend-money" placeholder="請輸入金額">
                                    </div>
                                </div>
                                <div class="form-group my-3">
                                    <label for="spend-category" class="col-md-2 control-label">分類</label>
                                    <div class="col-md-10">
                                        <select class="selectpicker">
                                                                        <option value="1" selected="selected">食品酒水</option>
                                                                        <option value="2">菸酒茶飲料</option>
                                                                        <option value="3">運動健身</option>
                                                                        <option value="4">行動交通</option>
                                                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-track-spending">確認記帳</button>
                                    </div>
                                </div>
                            </form>
                            <table class="table track-spending-table">
                                <tbody class="track-spending-table__item">
                                    <tr>
                                        <th rowspan="2"><img src="/dev/images/img_category-food.png" alt=""></th>
                                        <td style="vertical-align: bottom; font-size: 16px;">菸酒茶飲料 / $60</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                    </tr>
                                </tbody>
                                <tbody class="track-spending-table__item">
                                    <tr>
                                        <th rowspan="2"><img src="/dev/images/img_category-fun.png" alt=""></th>
                                        <td style="vertical-align: bottom; font-size: 16px;">運動健身 / $1600</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                    </tr>
                                </tbody>
                                <tbody class="track-spending-table__item">
                                    <tr>
                                        <th rowspan="2"><img src="/dev/images/img_category-traffic.png" alt=""></th>
                                        <td style="vertical-align: bottom; font-size: 16px;">行動交通 / $70</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <td colspan="2"><a href="#" class="content-more">更多 <i>&raquo;</i></a></td>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="o_content-box">
                        <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">理財新知</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                        <div class="media-container media--full-height">
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">跟朋友要錢就靠 「它」出國記帳才能理直氣壯『算帳』！</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">3500 萬發票獎金竟然沒有我的份？全是因為你 沒看「完全攻略」
                                        </h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-10.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">林銘水：我不是富二代 跟錢有關的事只能靠自己</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-11.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">成為單親媽媽 讓我一夜長大</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                            <div class="media m_media media--horizontal">
                                <figure class="media-left media__photo">
                                    <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-12.jpg" alt="img-thumbnail-2"></a>
                                    <figcaption class="media__photo-label">Best</figcaption>
                                </figure>
                                <div class="media-body">
                                    <a href="#">
                                        <h2 class="media-heading media_heading2">張怡婷：奢侈品無法讓你增值</h2>
                                    </a>
                                    <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                    <p class="media__date">2017-09-25</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-circle hidden-md" data-toggle="modal" data-target="#myModal">
                  
                    </button>

                    <!-- Modal -->
                    <div class="modal fade slide-from-btm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="track-spending--mobile">
                                    <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">快速記帳</span>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </h1>

                                    <div class="track-spending_container">
                                        <form class="form-horizontal track-spending-input my-3">
                                            <div class="form-group">
                                                <label for="spend-date" class="col-md-2 control-label">日期</label>
                                                <div class="col-md-10">
                                                    <input type="text" style="min-width:96%" class="form-control" id="spend-date" placeholder="請輸入日期">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="spend-money" class="col-md-2 control-label">金額</label>
                                                <div class="col-md-10">
                                                    <input type="text" class="form-control" id="spend-money" placeholder="請輸入金額">
                                                </div>
                                            </div>
                                            <div class="form-group my-3">
                                                <label for="spend-category" class="col-md-2 control-label">分類</label>
                                                <div class="col-md-10">
                                                      <select class="selectpicker">
                                                                        <option value="1" selected="selected">食品酒水</option>
                                                                        <option value="2">菸酒茶飲料</option>
                                                                        <option value="3">運動健身</option>
                                                                        <option value="4">行動交通</option>
                                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="button" class="btn btn-track-spending">確認記帳</button>
                                                </div>
                                            </div>
                                        </form>
                                        <table class="table track-spending-table">
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-food.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">菸酒茶飲料 / $60</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-fun.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">運動健身 / $1600</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tbody class="track-spending-table__item">
                                                <tr>
                                                    <th rowspan="2"><img src="/dev/images/img_category-traffic.png" alt=""></th>
                                                    <td style="vertical-align: bottom; font-size: 16px;">行動交通 / $70</td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <td colspan="2"><a href="#" class="content-more">更多 <i>&raquo;</i></a></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- endbuild -->
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/locale/zh-tw.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.js"></script>

<script>
    window.onload = function() {
        var todayDate = moment().startOf('day');
        var YM = todayDate.format('YYYY-MM');
        var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
        var TODAY = todayDate.format('YYYY-MM-DD');
        var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

        $(document).ready(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                // put your options and callbacks here
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                navLinks: true,
                events: [{
                        title: '轉帳：$6000',
                        start: YM + '-30'
                    },
                    {
                        title: '支出：$1000',
                        start: YM + '-30'
                    }, {
                        title: '收入：$10000',
                        start: YM + '-30'
                    }
                ]
            });

        });
    };

</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.css">

</html>
