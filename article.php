<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="article">
    <div class="sticky-content">
        <?php
            include "dev/php/header-is-not-login.php";
        ?>
        <div class="container fix-little-content-width">
            <div class="row article_first-row">
                <div class="col-md-12">
                    <div class="o_content-box article-inner">
                        <ol class="breadcrumb article-breadcrumb">
                            <li class="article-breadcrumb__item"><a href="/index.php">首頁</a></li>
                            <li class="article-breadcrumb__item"><a href="#" class="article__channel">房地產</a></li>
                        </ol>
                        <section class="article_column_2">
                            <div class="row">
                                <div class="col-sm-8">
                                    <article class="article">
                                        <header>
                                            <h1 class="article__title">2018房市大調查 57%受訪者不打算買房</h1>
                                            <div class="article__meta">
                                                <span class="article__meta-source">Money錢</span>
                                                <time class="article__meta-time" datetime="2017-12-13">2017-12-13</time>
                                            </div>
                                            <div class="article__meta">
                                                <span class="article__meta-author">撰文：劉育菁</span>
                                                <span class="article__meta-browse-times">瀏覽人次：25,687</span>
                                            </div>

                                        </header>

                                        <div class="article__img"></div>
                                        <div class="article__toolbar " style="">
        																		<div class="font-size-selector">
        																				<a href=""  onclick="event.preventDefault();fontResize('.article__text','low');" id="jq-font-mius">小</a>
        																				<a href=""  onclick="event.preventDefault();fontResize('.article__text','up');" id="jq-font-plus">大</a>
        																		</div>
        																		<ul class="social-share">
        																				<li>
        																						<div class="fb-like" data-href="<?=$self_url?>" data-layout="box_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
        																				</li>

        																				<li>
        																						<li><a class="fb-share" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($self_url)?>&amp;src=sdkpreparse', '_blank', 'width = 700, height = 650');" href="javascript: void(0)"><span class="social-share_position_adjust_fb"><span class="icon icon--facebook"></span><br>分享</span></a></li>
        																				</li>
        																				<li>
        																						<div class="line-it-button" data-lang="zh_Hant" data-type="share-d" data-url="<?= $self_url?>" style="display: none;"></div>
        																				</li>
        																				<!-- <li><a class="member-bookmark" href="#"><span class="social-share_position_adjust_member-bookmark"><span class="icon icon--star"></span><br>收藏</span></a></li> -->
        																				<li><a class="back-to-top" href="#" onclick="event.preventDefault();scrollToTop();"><span class="social-share_position_adjust_back-to-top"><span class="icon icon--arrow-up"></span></span></a></li>
        																		</ul>
        																</div>
                                        <div id="fakeToolbar" style="display:none"></div>
                                        <div class="article__text">
                                            <p>投資客超級心酸的2017年，展望2018年，台灣房地產又該何去何走？在央行信用管制鬆綁、超低利率的環境下，明年房地產有機會拉出長紅嗎？根據屋比趨勢研究中心市場調查，對2018年房地產的看法，有57%不會進場買房，只有不到3成考慮買房。有意願進場購屋者，決定買房的前3大原因，第1名是自住、換屋，約占35%；其次是資產保值約23%；第3名是結婚，約占18%，也意謂投資客並未積極進場。 此外，受訪者中認為2018年房價不會持續上漲的占比約54%，認為2018年房價有機會上漲的占比不到3成，看漲的原因包括政府不再打房（占29%）、房地產能夠保值（占25%），其他原因是利率太低及房地產可以保值（分別占14%）。 看空房地產後市的受訪者比重不低，認為房地產不再上漲主因是薪水沒增加（占33%），其次是台灣人口紅利結束（占24%），最後是房屋持有稅（地價稅及房屋稅）加重（約占22%）。 屋比趨勢研究中心總監陳傑鳴表示，台灣房地產進入結構調整期，未來房地產不太容易暴漲暴跌，房價修正相對合理時，自住客只要嚴格管控房貸占家庭可支配所得三分之一，達成購屋夢想，指日可待。</p>
                                            <p>對於物價的預估，他預估今年台灣通膨率（CPI）約0.7%，明年雖微調至1.2%，但仍在相對低檔區，也因此央行貨幣政策維持平穩，預期2018年央行利率將維持不動。 相較台灣利率及物價相對平穩，楊宇霆提醒投資人特別注意人民幣的「波動」。他說，2018年中國經濟成長率6.5%，跟今年差異並不大，新經濟對總體經濟貢獻占比將提高，抵消了去槓桿的經濟成本。 預估2018年美元兌人民幣匯率6.55，雖然與今年預估的6.6只差一點點，「但明年觀察人民幣重視『波動』勝於方向。」他強調，中國央行5月加入逆周期因子後，人民幣匯率更貼近美元指數的變化，未來人民幣走勢跟全球更緊密，反而要小心人民幣的匯率波動。
                                            </p>
                                            <span>
                                              對於物價的預估，他預估今年台灣通膨率（CPI）約0.7%，明年雖微調至1.2%，但仍在相對低檔區，也因此央行貨幣政策維持平穩，預期2018年央行利率將維持不動。 相較台灣利率及物價相對平穩，楊宇霆提醒投資人特別注意人民幣的「波動」。他說，2018年中國經濟成長率6.5%，跟今年差異並不大，新經濟對總體經濟貢獻占比將提高，抵消了去槓桿的經濟成本。 預估2018年美元兌人民幣匯率6.55，雖然與今年預估的6.6只差一點點，「但明年觀察人民幣重視『波動』勝於方向。」他強調，中國央行5月加入逆周期因子後，人民幣匯率更貼近美元指數的變化，未來人民幣走勢跟全球更緊密，反而要小心人民幣的匯率波動。
                                            </span>
                                        </div>
                                        <div class="article__author-box author-box hidden">
                                            <img class="author-box__avatar" src="/dev/images/img_avatar.png" alt="">
                                            <div class="author-box__name">劉育菁</div>
                                            <div class="author-box__intro">
                                                成大新聞系碩士，Money錢雜誌編輯
                                            </div>
                                            <button type="button" class="author-box__btn btn-primary--ghost">訂閱</button>
                                            <div class="author-box__quote">
                                                希望帶妳進入商業世界的格鬥場，和你一起分析，拆解商業問題。每一件事情背後都有其商業邏輯 ，你了解這些邏輯就可以步步為營取得成功
                                            </div>
                                        </div>

                                    </article>
                                    <div class="recommend-tool">
                                        <h2 class="heading_border_left recommend-tool__heading"><span>理財工具推薦</span></h2>
                                        <a href="#">
                                            <img class="recommend-tool__img" src="/dev/images/icon_product-funds-machine.jpg" alt="">
                                        </a>
                                        <div class="recommend-tool__text flex-row">

                                                <div>
                                                    <div class="recommend-tool__title"><a href="#">下載『基金機器人A寶』 App</a></div>
                                                    <ul class="recommend-tool__intro">
                                                        <li>熱門基金馬上搜</li>
                                                        <li>基金績效輕鬆查</li>
                                                        <li>基本資料一目瞭然</li>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <button class="btn-ghost" type="button">下載</button>

                                                </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <aside class="article-aside">
                                        <div class="article-aside__ads-google ads-google hidden">
                                            <a href="#"> <img class="img-responsive" src="/dev/images/img_ads-google.png" alt=""></a>
                                        </div>
                                        <div class="article-aside__related-articles related-articles">
                                            <h2 class="heading_border_left"><span>延伸閱讀</span></h2>
                                            <ul class="related-articles__body">
                                                <li><a href="#">林奇芬：你退休後的醫療保障夠嗎
</a></li>
                                                <li><a href="#">呂忠達：明年股市仍有正報酬
</a></li>
                                                <li><a href="#">賣壓逐步消化 台股盤勢可望回穩

</a></li>
                                                <li><a href="#">台灣冠軍 這8檔績優股現在正好入手
</a></li>
                                                <li><a href="#">童再興：我的理財傳家寶</a></li>
                                            </ul>
                                        </div>
                                        <div class="article-aside__hot-articles hot-articles">
                                            <h2 class="heading_border_left"><span>全站熱門文章</span></h2>
                                            <ul class="hot-articles__body">
                                                <li><a href="#">刷卡消費 幫自己賺1個月薪水</a></li>
                                                <li><a href="#">退休最怕這5個意外 就算月領8萬也吃土</a></li>
                                                <li><a href="#">林奇芬：年金改革這樣做可勞資雙贏
</a></li>
                                                <li><a href="#">邱沁宜：如果你活到一百歲
</a></li>
                                                <li><a href="#">做到位 5年管理資產增400%</a></li>
                                            </ul>
                                        </div>

                                        <div class="article-aside__my-activity my-activity">
                                            <h2 class="heading_border_left"><span>理財講座活動</span></h2>
                                            <div class="my-activity__body">
                                                <div class="my-activity__img">

                                                </div>
                                                <h2 class="my-activity__title">
                                                    <a href="#">免費參加，好運旺旺來！《開運賺大錢》新刊發表會</a>
                                                </h2>
                                                <p class="my-activity__description">
                                                    限量50名！馬上線上報名，還可參加抽獎！老師將分享2018風...
                                                </p>
                                                <div class="my-activity__meta">
                                                    <time datetime="2017-12-19">2017-12-19</time>
                                                    <button type="button">我要報名！</button>
                                                </div>

                                            </div>


                                        </div>
                                    </aside>

                                </div>
                            </div>




                        </section>
                    </div>


                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <style type="text/css">
    		.article__toolbar.sticky {
    				position: fixed;
    				top: 70px;
    				z-index: 100;
    				border-top: 0;
    				background-color: white
    		}
    		/* 如果是手機 高度改變*/
    		@media screen and (max-width: 600px) {
    				.article__toolbar.sticky {
    						top: 50px;
    				}
    		}
    </style>
     <script>
    // article__toolbar 的 fixed特效
    function stickyCheck(top,left,width) {
    	var $toolbar = $('.article__toolbar') ;
    	var scrollTop = $(window).scrollTop();
    	var $fakeDiv = $('#fakeToolbar');
    	$fakeDiv.height($toolbar.outerHeight());
    	console.log($toolbar.outerHeight()+' '+$fakeDiv.height());
    	if (scrollTop > top) {
    		$toolbar.addClass('sticky');
    		$toolbar.css('width',width);
    		$toolbar.css('left',left);
    		$fakeDiv.show();
    	} else {
    		$toolbar.removeClass('sticky');
    		$fakeDiv.hide();
    	}
    }

    // 捲頂特效
    function scrollToTop() {
    		var $body = $('body, html');
    				$body.stop().animate({scrollTop:0}, 800, 'swing', function() {
    		});
    }

    // 指定區域變換font-size大小
    function fontResize( div , action , bound = 0 , sizeGap = 2 ) {
    	if( 0 == bound){
    		if( 'up' == action ){
    			bound = 26 ;
    		} else if( 'low' == action ){
    			bound = 10 ;
    		}
    	}
      // var $div = $(div).find('span') ;
      var $div = $(div) ;
    	var $size = parseInt($div.css('font-size')) ;

    	//如果是 放大
    	if( 'up' == action && $size + sizeGap <= bound ){
    		var $newSize = $size + sizeGap ;
    		setFontsize($div,$newSize) ;
        console.log('按了大');
    	} else if( 'low' == action && $size - sizeGap >= bound ){
    		var $newSize = $size - sizeGap ;
    		setFontsize($div,$newSize) ;
        console.log('按了大');
    	}
    	return false;
    }

    // 改變字體大小
    function setFontsize(div,newSize){
    	$(div).css('font-size',String(newSize)+'px ');
    	document.cookie = "_article_fontsize="+String(newSize)+"; path=/";
    }

    // 取得cookie
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return undefined;
    }

    //檢查 font-size cookie
    function checkFontsizeCookie(){
    	var x = getCookie('_article_fontsize');
    	if( x != undefined ){
    		var div = $('.article__text span');
    		setFontsize(div,x);
    	}
    }

    // 頁面讀取完成會自動跑的js
    $(function () {
    	$('[data-toggle="popover"]').popover();

    	//檢查 font-size cookie
    	checkFontsizeCookie();

    	// article__toolbar 的 fixed特效使用
    	var toolbarHeight = 67 ;
    	var navHeight = $('.navbar.navbar-default.navbar-fixed-top').height();
    	var stickyTop = $('.article__text').offset().top - navHeight - toolbarHeight;
    	var stickyLeft = $('.article__text').offset().left;
    	var stickyWidth = $('.article__text').width();
    	stickyCheck(stickyTop,stickyLeft,stickyWidth);
    	$(window).scroll(function() {
    		stickyCheck(stickyTop,stickyLeft,stickyWidth);
    	});
    	// article__toolbar 的 fixed特效使用 end

      // $('.article__text span').css('font-size','')
    	//讀取 cookie中的article_fontsize
    })
    </script>
    <!-- endbuild -->


</body>

</html>
