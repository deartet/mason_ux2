<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->
</head>

<body id="vault-accounts">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container fix-little-content-width">
            <div class="row login_first-row">
                <div class="col-md-12">
                    <div class="o_content-box">
                        <section>
                            <header class="account-detail">
                                <h1 class="m_heading1"><span class="icon icon--bankbook"></span><span class="m_heading1__title">帳戶清單</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                                <ul class="total-asset-stat text-right">
                                    <li><span class="account-detail-title">淨資產：</span><span class="color-income">$479,000</span></li>
                                    <li><span class="account-detail-title">總資產：</span><span class="color-income">$480,000</span></li>
                                    <li><span class="account-detail-title">總負債：</span><span class="color-spend">$100</span></li>
                                </ul>
                            </header>

                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-primary" role="tablist" style="margin-top: 35px;">
                                <li role="presentation" class="active"><a href="#account-cash" aria-controls="spend" role="tab" data-toggle="tab">現金</a></li>
                                <li role="presentation"><a href="#account-asset" aria-controls="income" role="tab" data-toggle="tab">資產</a></li>
                                <li role="presentation"><a href="#account-stored-value" aria-controls="transfer" role="tab" data-toggle="tab">儲值卡</a></li>
                                <li role="presentation"><a href="#liability" aria-controls="mostFrequent" role="tab" data-toggle="tab">負債</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="account-cash">
                                    <div class="">
                                        <table class="table table-striped account-detail-table">
                                            <thead>
                                                <tr>
                                                    <th>帳戶</th>
                                                    <th>幣值</th>
                                                    <th style="text-align: center;">帳戶金額</th>
                                                    <th style="text-align: center;">初始金額</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                                <tr>
                                                    <td>現金</td>
                                                    <td>美元</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$100</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>

                                                <tr class="dropdown-tablerow">
                                                    <td colspan="5">
                                                        <form class="form-inline track-spending-form inside">
                                                            <div class="form-group track-spending-input">
                                                                <label for="amount" class="control-label">帳戶</label>
                                                                <input type="text" class="form-control" id="amount" placeholder="請輸入帳戶">
                                                            </div>
                                                            <div class="form-group track-spending-input">
                                                                <label for="account" class="control-label">幣值</label>
                                                                <input type="text" class="form-control" id="account" placeholder="請輸入幣值">
                                                            </div>
                                                            <div class="form-group track-spending-input">
                                                                <label for="receipt" class="control-label">帳戶金額</label>
                                                                <input type="text" class="form-control" id="receipt" placeholder="請輸入帳戶金額">
                                                            </div>
                                                            <div class="form-group track-spending-input">
                                                                <label for="category" class="control-label">初始金額</label>
                                                                <input type="text" class="form-control" id="category" placeholder="請輸入初始金額">
                                                            </div>
                                                            <div class="form-group track-spending-input">
                                                                <label for="tag" class="control-label">備註</label>
                                                                <input type="text" class="form-control" id="tag" placeholder="請輸入備註">
                                                            </div>
                                                            <div class="form-group" style="display: block;">
                                                                <button type="button" class="btn btn-common">重整</button>
                                                                <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;">確認</button>
                                                            </div>
                                                            <div class="form-group" style="display: block;margin-top: 10px;">
                                                                <button type="button" class="btn btn-default btn-delete">刪除</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <td>現金</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$55,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$120</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>現金</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$100</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1020</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>現金</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$120</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix">
                                        <?php 
                                            include "dev/php/modal-add-account.php"; 
                                        ?>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="account-asset">
                                    <div class="">
                                        <table class="table table-striped account-detail-table">
                                            <thead>
                                                <tr>
                                                    <th>帳戶</th>
                                                    <th>幣值</th>
                                                    <th style="text-align:center;">帳戶金額</th>
                                                    <th style="text-align:center;">初始金額</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                                <tr>
                                                    <td>富邦銀行</td>
                                                    <td>美元</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$120</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>富邦銀行</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$120</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>富邦銀行</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>富邦銀行</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$20</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-default btn-primary pull-right" data-toggle="modal" data-target="#addAccount-asset"><span class="icon-confirm"></span>新增帳戶</button>
                                        <!-- Modal -->
                                        <div class="modal fade slide-from-btm" id="addAccount-asset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="track-spending--mobile">
                                                        <h1 class="m_heading1"><span class="icon icon--bankbook"></span><span class="m_heading1__title">新增帳戶</span>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </h1>
                                                        <div class="track-spending_container">
                                                            <form class="form-horizontal track-spending-input my-3">
                                                                <div class="form-group">
                                                                    <label for="account-type" class="col-md-2 control-label">帳戶類型</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                    <option value="1" selected="selected">該tab默認帳戶</option>
                                    <option value="2">資產</option>
                                    <option value="3">儲值卡</option>
                                    <option value="4">負債</option>
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="account-name" class="col-md-2 control-label">帳戶名稱</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="account-name" placeholder="請輸入帳戶名稱">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group my-3">
                                                                    <label for="currency" class="col-md-2 control-label">幣值</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                    <option value="1">新台幣</option>
                                    <option value="2">美元</option>
                                    <option value="3">歐元</option>
                                    <option value="4">人民幣</option>
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="initial-amount" class="col-md-2 control-label">初始金額</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="initial-amount" placeholder="請輸入金額">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="note" class="col-md-2 control-label">備註</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="note" placeholder="請輸入備註">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-offset-2 col-sm-10">
                                                                        <button type="button" class="btn btn-track-spending">確認新增</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="account-stored-value">
                                    <div class="">
                                        <table class="table table-striped account-detail-table">
                                            <thead>
                                                <tr>
                                                    <th>帳戶</th>
                                                    <th>幣值</th>
                                                    <th style="text-align:center;">帳戶金額</th>
                                                    <th style="text-align:center;">初始金額</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                                <tr>
                                                    <td>悠遊卡</td>
                                                    <td>美元</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>悠遊卡</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$100</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>悠遊卡</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$1320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>悠遊卡</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-default btn-primary pull-right" data-toggle="modal" data-target="#addAccount-store-value"><span class="icon-confirm"></span>新增帳戶</button>
                                        <!-- Modal -->
                                        <div class="modal fade slide-from-btm" id="addAccount-store-value" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="track-spending--mobile">
                                                        <h1 class="m_heading1"><span class="icon icon--bankbook"></span><span class="m_heading1__title">新增帳戶</span>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </h1>
                                                        <div class="track-spending_container">
                                                            <form class="form-horizontal track-spending-input my-3">
                                                                <div class="form-group">
                                                                    <label for="account-type" class="col-md-2 control-label">帳戶類型</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                                                            <option value="1" selected="selected">該tab默認帳戶</option>
                                                                            <option value="2">資產</option>
                                                                            <option value="3">儲值卡</option>
                                                                            <option value="4">負債</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="account-name" class="col-md-2 control-label">帳戶名稱</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="account-name" placeholder="請輸入帳戶名稱">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group my-3">
                                                                    <label for="currency" class="col-md-2 control-label">幣值</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                                                            <option value="1">新台幣</option>
                                                                            <option value="2">美元</option>
                                                                            <option value="3">歐元</option>
                                                                            <option value="4">人民幣</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="initial-amount" class="col-md-2 control-label">初始金額</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="initial-amount" placeholder="請輸入金額">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="note" class="col-md-2 control-label">備註</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="note" placeholder="請輸入備註">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-offset-2 col-sm-10">
                                                                        <button type="button" class="btn btn-track-spending">確認新增</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade px-3" id="liability">
                                    <div class="">
                                        <table class="table table-striped account-detail-table">
                                            <thead>
                                                <tr>
                                                    <th>帳戶</th>
                                                    <th>幣值</th>
                                                    <th style="text-align:center;">帳戶金額</th>
                                                    <th style="text-align:center;">初始金額</th>
                                                    <th>備註</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    <td>中信信用卡</td>
                                                    <td>美元</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$100</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>車貸</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$120</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>車貸</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$20</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>

                                                </tr>
                                                <tr>
                                                    <td>車貸</td>
                                                    <td>新台幣</td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$5,320</div>
                                                    </td>
                                                    <td class="table-num-align-center-right">
                                                        <div>$50</div>
                                                    </td>
                                                    <td class="text-ellipsis">家庭雜用</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-default btn-primary pull-right" data-toggle="modal" data-target="#addAccount-liability"><span class="icon-confirm"></span>新增帳戶</button>
                                        <!-- Modal -->
                                        <div class="modal fade slide-from-btm" id="addAccount-liability" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="track-spending--mobile">
                                                        <h1 class="m_heading1"><span class="icon icon--bankbook"></span><span class="m_heading1__title">新增帳戶</span>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </h1>
                                                        <div class="track-spending_container">
                                                            <form class="form-horizontal track-spending-input my-3">
                                                                <div class="form-group">
                                                                    <label for="account-type" class="col-md-2 control-label">帳戶類型</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                                                            <option value="1" selected="selected">該tab默認帳戶</option>
                                                                            <option value="2">資產</option>
                                                                            <option value="3">儲值卡</option>
                                                                            <option value="4">負債</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="account-name" class="col-md-2 control-label">帳戶名稱</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="account-name" placeholder="請輸入帳戶名稱">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group my-3">
                                                                    <label for="currency" class="col-md-2 control-label">幣值</label>
                                                                    <div class="col-md-10">
                                                                        <select class="input-sm form-select">
                                                                            <option value="1">新台幣</option>
                                                                            <option value="2">美元</option>
                                                                            <option value="3">歐元</option>
                                                                            <option value="4">人民幣</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="initial-amount" class="col-md-2 control-label">初始金額</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="initial-amount" placeholder="請輸入金額">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="note" class="col-md-2 control-label">備註</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="form-control" id="note" placeholder="請輸入備註">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-offset-2 col-sm-10">
                                                                        <button type="button" class="btn btn-track-spending">確認新增</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-circle hidden-md" data-toggle="modal" data-target="#myModal"></button>

                        <!-- Modal -->
                        <div class="modal fade slide-from-btm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="track-spending--mobile">
                                        <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">快速記帳</span>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </h1>

                                        <div class="track-spending_container">
                                            <form class="form-horizontal track-spending-input my-3">
                                                <div class="form-group">
                                                    <label for="spend-date" class="col-md-2 control-label">日期</label>
                                                    <div class="col-md-10">
                                                        <input type="text" style="min-width:96%" class="form-control" id="spend-date" placeholder="請輸入日期">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="spend-money" class="col-md-2 control-label">金額</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" id="spend-money" placeholder="請輸入金額">
                                                    </div>
                                                </div>
                                                <div class="form-group my-3">
                                                    <label for="spend-category" class="col-md-2 control-label">分類</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" id="spend-category" placeholder="請輸入分類">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="button" class="btn btn-track-spending">確認記帳</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <table class="table track-spending-table">
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-food.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">菸酒茶飲料 / $60</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-fun.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">運動健身 / $1600</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tbody class="track-spending-table__item">
                                                    <tr>
                                                        <th rowspan="2"><img src="/dev/images/img_category-traffic.png" alt=""></th>
                                                        <td style="vertical-align: bottom; font-size: 16px;">行動交通 / $70</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; color: #BDBDBD;">2017/09/20</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <td colspan="2"><a href="#" class="content-more">更多 <i>&raquo;</i></a></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
