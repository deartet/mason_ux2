<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="vault-track-spending">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container fix-little-content-width">
            <div class="row login_first-row">
                <div class="col-md-12">
                    <div class="o_content-box">
                        <section>
                            <h1 class="m_heading1"><span class="icon icon--track-spending"></span><span class="m_heading1__title">記帳</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>

                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-primary" role="tablist">
                                <li role="presentation" class="active"><a href="#spend" aria-controls="spend" role="tab" data-toggle="tab">支出</a></li>
                                <li role="presentation"><a href="#income" aria-controls="income" role="tab" data-toggle="tab">收入</a></li>
                                <li role="presentation"><a href="#transfer" aria-controls="transfer" role="tab" data-toggle="tab">轉帳</a></li>
                                <li role="presentation"><a href="#mostFrequent" aria-controls="mostFrequent" role="tab" data-toggle="tab">常用</a></li>
                                <li role="presentation"><a href="#batchTrackSpending" aria-controls="batchTrackSpending" role="tab" data-toggle="tab">批次記帳</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="spend">
                                    <form class="form-inline my-3 mt-3 track-spending-form">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">
                                                <!-- Single button -->
                                                <div class="btn-group btn-camera-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                </button>
                                                    <ul class="dropdown-menu">
                                                        <li class=""><a href="#">拍照</a></li>
                                                        <li><a href="#">上傳照片</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-11">
                                                <div class="form-group track-spending-input">
                                                    <label for="amount" class="control-label">金額</label>
                                                    <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="account" class="control-label">帳戶</label>
                                                    <input type="text" class="form-control" id="account" placeholder="現金（選單）">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="receipt" class="control-label">發票</label>
                                                    <input type="text" class="form-control" id="receipt" placeholder="請輸入發票後三碼">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="category" class="control-label">分類</label>
                                                    <input type="text" class="form-control" id="category" placeholder="請輸入分類（選單）">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="tag" class="control-label">標籤</label>
                                                    <input type="text" class="form-control" id="tag" placeholder="請註明專案，如出差">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="time" class="control-label">時間</label>
                                                    <input type="text" class="form-control" id="time" placeholder="要精確到月日時分">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="note" class="control-label">備註</label>
                                                    <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="location" class="control-label">位置</label>
                                                    <input type="text" class="form-control" id="location" placeholder="默認地標">
                                                </div>
                                                <div class="form-group" style="display: block;">
                                                    <button type="button" class="btn btn-common active"><span class="icon-common"></span>存為常用</button>
                                                    <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;"><span class="icon-confirm"></span>確認記帳</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="common-template spend">
                                        <li>Friday餐廳</li>
                                        <li>雞排腿排</li>
                                        <li>貢丸麵</li>
                                        <li>50嵐 四季奶青</li>
                                        <li>西門町 定食吧</li>
                                        <li>7-11早餐 香蕉</li>
                                        <li>隔壁 熱炒</li>
                                        <li>仙草甘茶加珍珠</li>
                                        <li>遠傳 手機通話費</li>
                                        <li>機車 加油費</li>
                                    </ul>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="income">
                                    <form class="form-inline my-3 mt-3 track-spending-form">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">
                                                <!-- Single button -->
                                                <div class="btn-group btn-camera-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                </button>
                                                    <ul class="dropdown-menu">
                                                        <li class=""><a href="#">拍照</a></li>
                                                        <li><a href="#">上傳照片</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-11">
                                                <div class="form-group track-spending-input">
                                                    <label for="amount" class="control-label">金額</label>
                                                    <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="account" class="control-label">帳戶</label>
                                                    <input type="text" class="form-control" id="account" placeholder="現金（選單）">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="receipt" class="control-label">發票</label>
                                                    <input type="text" class="form-control" id="receipt" placeholder="請輸入發票後三碼">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="category" class="control-label">分類</label>
                                                    <input type="text" class="form-control" id="category" placeholder="請輸入分類（選單）">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="tag" class="control-label">標籤</label>
                                                    <input type="text" class="form-control" id="tag" placeholder="請註明專案，如出差">
                                                </div>

                                                <div class="form-group track-spending-input">
                                                    <label for="time" class="control-label">時間</label>
                                                    <input type="text" class="form-control" id="time" placeholder="要精確到月日時分">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="note" class="control-label">備註</label>
                                                    <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="location" class="control-label">位置</label>
                                                    <input type="text" class="form-control" id="location" placeholder="默認地標">
                                                </div>
                                                <div class="form-group" style="display: block;">

                                                    <button type="button" class="btn btn-common active"><span class="icon-common"></span>存為常用</button>
                                                    <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;"><span class="icon-confirm"></span>確認記帳</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="common-template income">
                                        <li>薪水收入</li>
                                        <li>利息收入</li>
                                        <li>兼職收入</li>
                                        <li>中秋節禮金</li>
                                        <li>三節獎金</li>
                                        <li>打工收入</li>
                                        <li>賣書</li>
                                        <li>兼差收入</li>
                                        <li>年終</li>
                                        <li>薪水收入</li>
                                    </ul>


                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="transfer">
                                    <form class="form-inline my-3 mt-3 track-spending-form">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">
                                                <!-- Single button -->
                                                <div class="btn-group btn-camera-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                </button>
                                                    <ul class="dropdown-menu">
                                                        <li class=""><a href="#">拍照</a></li>
                                                        <li><a href="#">上傳照片</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-11">
                                                <div class="form-group track-spending-input">
                                                    <label for="amount" class="control-label">金額</label>
                                                    <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="export" class="control-label">轉出</label>
                                                    <input type="text" class="form-control" id="export" placeholder="請選擇帳戶（選單）">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="receipt" class="control-label">標籤</label>
                                                    <input type="text" class="form-control" id="receipt" placeholder="請註明專案，如出差">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="category" class="control-label">時間</label>
                                                    <input type="text" class="form-control" id="category" placeholder="精確到月日時分">
                                                </div>
                                                <div class="form-group track-spending-input">
                                                    <label for="import" class="control-label">轉入</label>
                                                    <input type="text" class="form-control" id="import" placeholder="請選擇帳戶（選單）">
                                                </div>

                                                <div class="form-group track-spending-input">
                                                    <label for="note" class="control-label">備註</label>
                                                    <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                </div>
                                                <div class="form-group" style="display: block;">
                                                    <button type="button" class="btn btn-common active"><span class="icon-common"></span>存為常用</button>
                                                    <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;"><span class="icon-confirm"></span>確認記帳</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="common-template">
                                        <li>轉匯薪資帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                        <li>轉匯信用卡帳戶</li>
                                    </ul>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="mostFrequent">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs common-template-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#common-spend" aria-controls="common-spend" role="tab" data-toggle="tab">支出</a></li>
                                        <li role="presentation"><a href="#common-income" aria-controls="#common-income" role="tab" data-toggle="tab">收入</a></li>
                                        <li role="presentation"><a href="#common-transfer" aria-controls="profile" role="tab" data-toggle="tab">轉帳</a></li>
                                        <li class="pull-right"><button type="button" class="btn btn-default btn-add-common"><span class="icon-confirm"></span>常用</button></li>
                                    </ul>





                                    <!-- Tab panes -->
                                    <div class="tab-content common-template-tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="common-spend">
                                            <div class="table-responsive scrolling-hints">
                                                <table class="table table-striped account-detail-table" style="margin-top: 15px; position: relative;">

                                                    <thead>
                                                        <tr style="cursor: initial;">
                                                            <th style="padding-left:0; padding-right:0;">
                                                                &nbsp;
                                                            </th>
                                                            <th>&nbsp;</th>
                                                            <th><a href="#" class="table-filter">名稱（備註）</a></th>
                                                            <th><a href="#" class="table-filter">分類</a></th>
                                                            <th><a href="#" class="table-filter">帳戶</a></th>
                                                            <th style="text-align: center;"><a href="#" class="table-filter">金額</a></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td><img src="/dev/images/icon_camera.svg" height="19px" alt=""></td>

                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr class="dropdown-tablerow">
                                                            <td colspan="6">
                                                                <form class="form-inline track-spending-form">
                                                                    <div class="row">
                                                                        <div class="col-md-1">
                                                                            <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">

                                                                            <div class="btn-group btn-camera-group">
                                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                                </button>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class=""><a href="#">拍照</a></li>
                                                                                    <li><a href="#">上傳照片</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-11">
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="amount" class="control-label">金額</label>
                                                                                <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="account" class="control-label">帳戶</label>
                                                                                <input type="text" class="form-control" id="account" placeholder="現金（選單）">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="receipt" class="control-label">發票</label>
                                                                                <input type="text" class="form-control" id="receipt" placeholder="請輸入發票後三碼">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="category" class="control-label">分類</label>
                                                                                <input type="text" class="form-control" id="category" placeholder="請輸入分類（選單）">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="tag" class="control-label">標籤</label>
                                                                                <input type="text" class="form-control" id="tag" placeholder="請註明專案，如出差">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="time" class="control-label">時間</label>
                                                                                <input type="text" class="form-control" id="time" placeholder="要精確到月日時分">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="note" class="control-label">備註</label>
                                                                                <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="location" class="control-label">位置</label>
                                                                                <input type="text" class="form-control" id="location" placeholder="默認地標">
                                                                            </div>
                                                                            <div class="form-group" style="display: block;">
                                                                                <button type="button" class="btn btn-common">取消</button>
                                                                                <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;">保存</button>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <button type="button" style="margin-top: 12px;" class="btn btn-default btn-delete">刪除</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_spend.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <nav aria-label="Page navigation" class="text-right">
                                                <ul class="pagination">
                                                    <li>
                                                        <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
                                                    </li>
                                                    <li><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#">5</a></li>
                                                    <li>
                                                        <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="common-income">
                                            <div class="table-responsive scrolling-hints">
                                                <table class="table table-striped account-detail-table" style="margin-top: 15px; position: relative;">

                                                    <thead>
                                                        <tr style="cursor: initial;">
                                                            <th style="padding-left:0; padding-right:0;">
                                                                &nbsp;
                                                            </th>
                                                            <th>&nbsp;</th>
                                                            <th><a href="#" class="table-filter">名稱（備註）</a></th>
                                                            <th><a href="#" class="table-filter">分類</a></th>
                                                            <th><a href="#" class="table-filter">帳戶</a></th>
                                                            <th style="text-align: center;"><a href="#" class="table-filter">金額</a></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td><img src="/dev/images/icon_camera.svg" height="19px" alt=""></td>

                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr class="dropdown-tablerow">
                                                            <td colspan="6">
                                                                <form class="form-inline track-spending-form">
                                                                    <div class="row">
                                                                        <div class="col-md-1">
                                                                            <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">

                                                                            <div class="btn-group btn-camera-group">
                                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                                </button>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class=""><a href="#">拍照</a></li>
                                                                                    <li><a href="#">上傳照片</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-11">
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="amount" class="control-label">金額</label>
                                                                                <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="account" class="control-label">帳戶</label>
                                                                                <input type="text" class="form-control" id="account" placeholder="現金（選單）">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="receipt" class="control-label">發票</label>
                                                                                <input type="text" class="form-control" id="receipt" placeholder="請輸入發票後三碼">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="category" class="control-label">分類</label>
                                                                                <input type="text" class="form-control" id="category" placeholder="請輸入分類（選單）">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="tag" class="control-label">標籤</label>
                                                                                <input type="text" class="form-control" id="tag" placeholder="請註明專案，如出差">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="time" class="control-label">時間</label>
                                                                                <input type="text" class="form-control" id="time" placeholder="要精確到月日時分">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="note" class="control-label">備註</label>
                                                                                <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="location" class="control-label">位置</label>
                                                                                <input type="text" class="form-control" id="location" placeholder="默認地標">
                                                                            </div>
                                                                            <div class="form-group" style="display: block;">
                                                                                <button type="button" class="btn btn-default btn-common">取消</button>
                                                                                <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;">保存</button>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <button type="button" style="margin-top: 12px;" class="btn btn-default btn-delete">刪除</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_income.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>
                                                            <td>菸酒茶飲料</td>
                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <nav aria-label="Page navigation" class="text-right">
                                                <ul class="pagination">
                                                    <li>
                                                        <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
                                                    </li>
                                                    <li><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#">5</a></li>
                                                    <li>
                                                        <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="common-transfer">
                                            <div class="table-responsive scrolling-hints">
                                                <table class="table table-striped account-detail-table" style="margin-top: 15px; position: relative;">

                                                    <thead>
                                                        <tr style="cursor: initial;">
                                                            <th style="padding-left:0; padding-right:0;">
                                                                &nbsp;
                                                            </th>
                                                            <th>&nbsp;</th>
                                                            <th><a href="#" class="table-filter">名稱（備註）</a></th>

                                                            <th><a href="#" class="table-filter">帳戶</a></th>
                                                            <th style="text-align: center;"><a href="#" class="table-filter">金額</a></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td><img src="/dev/images/icon_camera.svg" height="19px" alt=""></td>

                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr class="dropdown-tablerow">
                                                            <td colspan="5">
                                                                <form class="form-inline track-spending-form">
                                                                    <div class="row">
                                                                        <div class="col-md-1">
                                                                            <img src="/dev/images/img_default.png" class="img-responsive my-1" style="max-height: 64px;" alt="">

                                                                            <div class="btn-group btn-camera-group">
                                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span class="icon icon--camera"></span><span class="caret"></span>
                                                                </button>
                                                                                <ul class="dropdown-menu">
                                                                                    <li class=""><a href="#">拍照</a></li>
                                                                                    <li><a href="#">上傳照片</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-11">
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="amount" class="control-label">金額</label>
                                                                                <input type="text" class="form-control" id="amount" placeholder="請輸入金額">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="export" class="control-label">轉出</label>
                                                                                <input type="text" class="form-control" id="export" placeholder="請選擇帳戶（選單）">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="receipt" class="control-label">標籤</label>
                                                                                <input type="text" class="form-control" id="receipt" placeholder="請註明專案，如出差">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="category" class="control-label">時間</label>
                                                                                <input type="text" class="form-control" id="category" placeholder="精確到月日時分">
                                                                            </div>
                                                                            <div class="form-group track-spending-input">
                                                                                <label for="import" class="control-label">轉入</label>
                                                                                <input type="text" class="form-control" id="import" placeholder="請選擇帳戶（選單）">
                                                                            </div>

                                                                            <div class="form-group track-spending-input">
                                                                                <label for="note" class="control-label">備註</label>
                                                                                <input type="text" class="form-control" id="note" placeholder="輸入備註或關鍵字">
                                                                            </div>
                                                                            <div class="form-group" style="display: block;">
                                                                                <button type="button" class="btn btn-common">取消</button>
                                                                                <button type="button" class="btn btn-default btn-primary" style="margin-left: 30px;">保存</button>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <button type="button" style="margin-top: 12px;" class="btn btn-default btn-delete">刪除</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td><img src="/dev/images/icon_transfer.svg" height="19px" alt=""></td>
                                                            <td>&nbsp;</td>
                                                            <td>出差台北請客喝的飲料</td>

                                                            <td>悠遊卡</td>
                                                            <td class="table-num-align-center-right">
                                                                <div>$33225</div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <nav aria-label="Page navigation" class="text-right">
                                                <ul class="pagination">
                                                    <li>
                                                        <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
                                                    </li>
                                                    <li><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#">5</a></li>
                                                    <li>
                                                        <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="batchTrackSpending">
                                    <form class="form-inline my-3 mt-3 track-spending-form form-batchTrackSpending">

                                        <div class="form-group track-spending-input">
                                            <label style="display: block;">第1筆</label>
                                            <input type="text" class="form-control" id="amount" placeholder="精確到月日時分">
                                            <input type="text" class="form-control" id="export" placeholder="請選擇帳戶（選單）">
                                            <input type="text" class="form-control" id="receipt" placeholder="請選擇分類">
                                            <input type="text" class="form-control" id="receipt" placeholder="輸入備註或關鍵字">
                                        </div>
                                        <div class="form-group track-spending-input">
                                            <label style="display: block;">第2筆</label>
                                            <input type="text" class="form-control" id="amount" placeholder="精確到月日時分">
                                            <input type="text" class="form-control" id="export" placeholder="請選擇帳戶（選單）">
                                            <input type="text" class="form-control" id="receipt" placeholder="請選擇分類">
                                            <input type="text" class="form-control" id="receipt" placeholder="輸入備註或關鍵字">
                                        </div>
                                        <div class="form-group track-spending-input">
                                            <label style="display: block;">第3筆</label>
                                            <input type="text" class="form-control" id="amount" placeholder="精確到月日時分">
                                            <input type="text" class="form-control" id="export" placeholder="請選擇帳戶（選單）">
                                            <input type="text" class="form-control" id="receipt" placeholder="請選擇分類">
                                            <input type="text" class="form-control" id="receipt" placeholder="輸入備註或關鍵字">
                                        </div>




                                        <div class="form-group" style="display: block;">

                                            <button type="button" class="btn btn-default btn-primary pull-left"><span class="icon-confirm"></span>確認記帳</button>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>

                    
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
