#資料夾結構簡介

1.  root folder的index.php
    位於root folder, 是為了讓server讀得到
    作用：就像dev phase的html一樣，是作為開發使用，裡面有css/js的source code，
    運用gulp task "useref" 把css/js 壓縮, cache和送到prod (production) folder

2.  modules:
    php - 用include將php partial化, 用gulp task "useref", 將有著minify的*.php pipe到prod (production) folder
    css - 用gulp task "useref" 將css partial化, 把css concatenate並uglify到prod (production) folder
    js  - 用gulp task "useref" 將js partial化, 把js concatenate並uglify到prod (production) folder 

3.  gulpfile內有以下gulp tasks：
    
    /****** DEVELOPMENT PHASE *****/
    
    gulp: dev - 把所有development phase的gulp tasks全部chain起來，在開發時只要run gulp dev就好了，ayw是建議用gulp defaults, 這樣只要run gulp
    
    gulp: watch - watch scss, js, html, and php - watch files for changes，只要有html/php檔案更動，後面的tasks就會run，這裡是browserSync跟compile sass
    
    gulp: compile-bootstrap - _bootstrap.scss不能利用gulp compile-sass task compile,但bootstrap scss source code有bootstrap.scss，所以暫時用不到
    
    gulp: broswerSync - php/wordpress的browserSync設定方式和html的不大一樣，因為browserSync會自建server,而php運行本身也需要使用MAMP創建server，因此只要使用MAMP的server就好，在proxy下指定要運行的proxy name，然後在MAMP Pro下創建同樣名字的server name，例如： proxy: "gulp-html-php-template-no-wp/", 並指定在php/wordpress在root folder所需的styles.css, "./styles.css"
    
    gulp: compile-sass - 用custom plumber function notify errors, 加sourcemaps for scss, compile sass to css, 加autoprefixer, 把css通過browserSync inject到browser, 注意：scss/modules/*.scss compile後會是在css/modules/*.css，需要修改*.php裡面的  <link rel="stylesheet" href="dev/css/modules/mystyles-*.css">
    
    gulp: spritesIMG - 將dev/images/sprites_img裡面的images sprite成一張圖，並產生scss檔案用來產生background position和width, height, 可生成retina版的image, @import "../_sprites_img" 之後，然後run .github的class來運用, 還要修改background img的 url:  imgPath: '/dev/images/sprites_img.png' 
   
    gulp: spritesSVG - 包含刪除舊的svg sprite, temp folder的svg的 gulp beginClean, 生成svg到temp folder的 gulp createspritessvg，複製temp folder的svg到dev/images的 gulp copyspritesgraphic, 複製產生的svg所需的css並且rename成 _sprites_svg.scss後送到dev/scss folder，和刪掉temp folder的 gulp endClean
    
    
    /****** OPTIMIZATION PHASE *****/
    
    gulp: production - 將所有在development phase和optimzation phase的gulp tasks全部chain起來，但不包含watch和browserSync,因為不需要了，用runSequence指定要run的順序！包含清除prod folder， sprite SVGs and Images， compile sass， useref壓縮css/js，optimize壓縮SVGs and Images的檔案大小，複製不須壓縮的字體，gulp production在ayw叫做gulp build，是完成所有工作時要run的command.
    
    gulp: debug - 識別什麼檔案經過 pipe stream，用來偵測tasks的運行狀況

    gulp: useref - useref意思是use html/php裡面的javascript files as reference，可將php/html裡面的module css, js partial化，然後concatenate and uglify後，將最新cache bust過的css/js送到任一資料夾
    gulp useref包含 gulp cssnano, 它是用來minify css檔案
    ***** uglify後的css/js並不一定要送到跟php/html的子目錄, 可用 build:js ../js/myscript.min.js 送到任一資料夾 *****
    
    gulp: uncss - 確保html檔案裡不使用的css selectors會被移除，在使用bootstrap等大型frameworks會使用 ---- 不實用
       
    gulp: newer - 比較dev和prod folder images的timestamps，如果prod folder images的比較新，就可以確定images已經被optimized

    gulp: optimages - 將png|jpg|jpeg|gif|svg檔案從dev/images和prod/images新的images(即sprite後的svg/img)壓縮檔案大小後再次送到prod/images,可調整png|jpg|jpeg|gif|svg的optimize參數，並用gulp newer辨識是否已經被optmized
    
    gulp: cached - 如果有多個html檔案，確保不是多個min.js，而是只有一個min.js進入uglify()
    
    gulp: rev / rev-place - 瀏覽器會通過將assets - images,css,js cache在瀏覽器裡面，但瀏覽器只要有asset，總是會試圖cache assets，但並不一定知道我們更新了assets. cache-busting是用來告訴瀏覽器，assets已經有新版了，通過用gulp rev加hash key在後面的方式
    rev - 改assets的file name
    rev-place - 替換舊hash key的asset files
    
     
    gulp: copyFonts - 不需要壓縮，直接複製到prod folder
    
    gulp: clean:production - 刪掉所有在development phase生成送到prod folder的檔案，避免在production server加入不必要的檔案，也是為了在run gulp prodution command時有最新的prod folder, 在clean的process不包含image, 避免花時間再optimize一遍已optimized的images
    
    gulp: browserSync:production - 在production phase時如果需要browserSync才需使用
    

    /****** DEPLOYMENT PHASE *****/
    
    deployment phase: 把檔案放到server讓大家看
    deploy有很多方法: 
    deploy with command line using gulp 
    
    use gulp ftp
    var creds - parse secrets.json相關properties and values給gulp task
    secrets.json - 存放server相關資訊
    gulp: ftp-clean - run ftp-clean，刪除要上傳的folder, ****記得改要刪除的檔案路徑！！！****
    gulp: ftp - run gulp ftp 上傳到server，記得改server端要上傳的資料夾，與要刪除的是同一個路徑
    
    
    /****** SCAFFOLDING PHASE *****/
    
    create a git repo
        clone workflow temlate repository
            cd sites - 才會clone到正確的資料夾
            git clone https://xxx.git 正確的資料夾名稱
    記得換Proxy in gulpfile.js - BrowerSync才會work
        proxy: "新專案的名稱-要與MAMP Pro server同名/",
    更換git remote url
        刪除原來workflow remote origin
            git remote remove origin
        新增新的remote origin
            git remote add origin url-to-new-git-repo
    安裝dependencies - 相關dependecies已經儲存在package.json
        npm install
    更新dependencies
    新增secrets.json
        內含server相關資訊
        


