<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家-最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <!-- endbuild -->

</head>

<body id="home">
    <div class="sticky-content">
        <?php include "dev/php/header-is-not-login.php"; ?>
        <div class="container home_first-row">
            <div class="flex-row">
                <div class="flex-col-md-8 o_content-box">
                    <h1 class="m_heading1"><span class="icon icon--feature-articles"></span><span class="m_heading1__title">錢管家精選</span><span class="division--vertical invisible"></span><span class="m_heading1__title--english invisible">Staff Picks</span></h1>
                    <div class="media-container">
                        <div class="row">
                            <div class="col-sm-5">
                                <article class="m_media">
                                    <figure class="media__photo feature">
                                        <a href="/article.php">
                                            <img src="/dev/images/img-thumbnail-13.jpg" style="width: 100%; height: 172px;" alt="An awesome picture">
                                            </a>
                                        <figcaption class="media__photo-label">Best
                                        </figcaption>
                                        <div class="arrow-right c-rotate-45"></div>
                                    </figure>
                                    <div class="media__body clearfix">
                                      
                                        <h2 class="media_heading2"><a href="/article.php">2018房市大調查 57%受訪者不打算買房</a></h2>
                                        
                                        <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                        <p class="media__date">2017-12-13</p>
                                    </div>
                                </article>
                            </div>
                            <div class="col-sm-7">
                                <div class="media m_media media--horizontal">
                                    <figure class="media-left media__photo feature">
                                        <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-2.jpg" alt="img-thumbnail-2"></a>
                                        <figcaption class="media__photo-label">Best
                                        </figcaption>
                                        <div class="arrow-right c-rotate-45"></div>
                                    </figure>
                                    <div class="media-body">
                                       
                                            <h2 class="media-heading media_heading2"><a href="#">邱沁宜：台幣匯率波動 未來更刺激</a></h2>
                                    
                                        <p class="media__browse-times hidden-xxs"><span class="icon icon--view"></span>37240</p>
                                        <p class="media__date">2017-09-25</p>
                                    </div>
                                </div>
                                <div class="media m_media media--horizontal">
                                    <figure class="media-left media__photo feature">
                                        <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-3.jpg" alt="img-thumbnail-2"></a>
                                        <figcaption class="media__photo-label">Best
                                        </figcaption>
                                        <div class="arrow-right c-rotate-45"></div>
                                    </figure>
                                    <div class="media-body">
                                       
                                            <h2 class="media-heading media_heading2"><a href="#">林奇芬：如何用較少退休金過好生活</a></h2>
                                       
                                        <p class="media__browse-times hidden-xxs"><span class="icon icon--view"></span>37240</p>
                                        <p class="media__date">2017-09-25</p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-col-md-4 o_content-box">
                    <div class="receipt-a-payment">
                        <div class="current-receipt">
                            <h1 class="m_heading1"><span class="icon icon--receipt-lottery"></span><span class="m_heading1__title">當期發票</span><span class="division--vertical invisible"></span><span class="m_heading1__title--english invisible">Receipt Lottery</span></h1>
                            <div class="current-receipt-flex">
                                <div class="text-w-imgs">
                                    <img src="/dev/images/img_lottery@2x.png" alt=""><span>106年7-8月發票開獎囉！</span>

                                </div>
                                <button type="button" class="btn btn-secondary">立即對獎</button>
                            </div>
                        </div>
                        <div class="hot-payment">
                            <h1 class="m_heading1"><span class="icon icon--receipt-lottery"></span><span class="m_heading1__title">熱門繳費</span><span class="division--vertical invisible"></span><span class="m_heading1__title--english invisible">Hot Bills</span></h1>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_telephone-bill@2x.png" alt=""><a href="#"><span>遠傳電信電信費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_car-bill@2x.png" alt=""><a href="#"><span>汽機車燃料使用</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_water-bill@2x.png" alt=""><a href="#"><span>新北市水費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_healthinsurance-bill@2x.png" alt=""><a href="#"><span>健保費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_creditcard-bill@2x.png" alt=""><a href="#"><span>玉山銀行信用卡</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_water-bill@2x.png" alt=""><a href="#"><span>台北市水費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_telephone-bill@2x.png" alt=""><a href="#"><span>中華電信電信費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="text-w-imgs">
                                        <img src="/dev/images/img_car-bill@2x.png" alt=""><a href="#"><span>台北市停車費</span></a>
                                        <!--             限制字數為7個字            -->
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <p class="kgi-bank">本服務與<span class="kgi-bank__text">凱基銀行</span>共同維護，支援29家銀行帳戶繳費作業。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="o_content-box home_full-height-content-box home_section-money-mgmt">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>理財</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-5 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">理財新知</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                                <div class="media-container media--full-height">
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-4.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                          
                                                <h2 class="media-heading media_heading2"><a href="#">跟朋友要錢就靠 「它」出國記帳才能理直氣壯『算帳』！</a></h2>
                                        
                                            <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-5.png" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">3500 萬發票獎金竟然沒有我的份？全是因為你 沒看「完全攻略」
                                                </h2>
                                            </a>
                                            <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-6.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">「帳單沒繳」、「懶得記帳」！3 個理財壞習慣，搞亂你的腦袋？</h2>
                                            </a>
                                            <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <section class="home-moneymgmt-services-box-container">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--feature-services"></span><span class="m_heading2__title">三大理財服務</span></h2>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box mx-auto">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_track-spending.svg" alt="">
                                                        <h3>記帳</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">理財第一步</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box mx-auto">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_paybill.svg" alt="">
                                                        <h3>繳費</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">隨繳隨記</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="m_home-moneymgmt-services-box mx-auto">
                                                <a href="#">
                                                    <div class="home-moneymgmt-services-box__icon-box">
                                                        <img src="/dev/images/icon_receipt-lottery.svg" alt="">
                                                        <h3>發票</h3>
                                                    </div>
                                                </a>
                                                <p class="home-moneymgmt-services-box__heading">掃發票對獎</p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <?php include "dev/php/snippet-btn-get-started.php"; ?>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="o_content-box home_full-height-content-box home_section-insurance">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>保險</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-5 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--news"></span><span class="m_heading2__title">保險新知</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h2>
                                <div class="media-container media--full-height">
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-7.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">熱死人啦！ 炎炎夏日，不小心中暑的話，什麼險會賠？</h2>
                                            </a>
                                            <p class="media__browse-times"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-8.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">AppleCare+進不了台灣，竟然是因為「這個」！以後買手機 都要加買保險...
                                                </h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                    <div class="media m_media media--horizontal">
                                        <figure class="media-left media__photo">
                                            <a href="#"><img class="media-object" src="/dev/images/img-thumbnail-9.jpg" alt="img-thumbnail-2"></a>
                                            <figcaption class="media__photo-label">Best</figcaption>
                                        </figure>
                                        <div class="media-body">
                                            <a href="#">
                                                <h2 class="media-heading media_heading2">調整 保險支出，2年達成 買屋目標！</h2>
                                            </a>
                                            <p class="media__browse-times hidden-sm"><span class="icon icon--view"></span>37240</p>
                                            <p class="media__date">2017-09-25</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <section>
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--insurance"></span><span class="m_heading2__title">保險專區</span></h2>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">C/P值最高</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">最多人缺</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">最多人買</a></li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <div class="">
                                                <table class="table table-striped table-insurance">
                                                    <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>險種</th>
                                                            <th>公司</th>
                                                            <th>保險名稱</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-health"></span></th>
                                                            <td class="insurance-health">健康險</td>
                                                            <td>法國巴黎人壽</td>
                                                            <td>一年定期重大疾病健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-savings"></span></th>
                                                            <td class="insurance-savings">儲蓄險</td>
                                                            <td>富邦人壽</td>
                                                            <td>永福豁免保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-travel"></span></th>
                                                            <td class="insurance-travel">旅平險</td>
                                                            <td>友邦人壽</td>
                                                            <td>新幸福安康終身醫療健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-accident"></span></th>
                                                            <td class="insurance-accident">意外險</td>
                                                            <td>元大人壽</td>
                                                            <td>享安住院醫療健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-invest"></span></th>
                                                            <td class="insurance-invest">投資險</td>
                                                            <td>台灣人壽</td>
                                                            <td>珍安心一年期日額型保險</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="profile">
                                            <div class="">
                                                <table class="table table-striped table-insurance">
                                                    <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>險種</th>
                                                            <th>公司</th>
                                                            <th>保險名稱</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-savings"></span></th>
                                                            <td class="insurance-savings">儲蓄險</td>
                                                            <td>富邦人壽</td>
                                                            <td>永福豁免保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-health"></span></th>
                                                            <td class="insurance-health">健康險</td>
                                                            <td>法國巴黎人壽</td>
                                                            <td>一年定期重大疾病健康保險</td>
                                                        </tr>

                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-accident"></span></th>
                                                            <td class="insurance-accident">意外險</td>
                                                            <td>元大人壽</td>
                                                            <td>享安住院醫療健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-travel"></span></th>
                                                            <td class="insurance-travel">旅平險</td>
                                                            <td>友邦人壽</td>
                                                            <td>新幸福安康終身醫療健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-invest"></span></th>
                                                            <td class="insurance-invest">投資險</td>
                                                            <td>台灣人壽</td>
                                                            <td>珍安心一年期日額型保險</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="messages">
                                            <div class="">
                                                <table class="table table-striped table-insurance">
                                                    <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>險種</th>
                                                            <th>公司</th>
                                                            <th>保險名稱</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-travel"></span></th>
                                                            <td class="insurance-travel">旅平險</td>
                                                            <td>友邦人壽</td>
                                                            <td>新幸福安康終身醫療健康保險</td>
                                                        </tr>

                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-savings"></span></th>
                                                            <td class="insurance-savings">儲蓄險</td>
                                                            <td>富邦人壽</td>
                                                            <td>永福豁免保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-invest"></span></th>
                                                            <td class="insurance-invest">投資險</td>
                                                            <td>台灣人壽</td>
                                                            <td>珍安心一年期日額型保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-accident"></span></th>
                                                            <td class="insurance-accident">意外險</td>
                                                            <td>元大人壽</td>
                                                            <td>享安住院醫療健康保險</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"><span class="icon icon--insurance-health"></span></th>
                                                            <td class="insurance-health">健康險</td>
                                                            <td>法國巴黎人壽</td>
                                                            <td>一年定期重大疾病健康保險</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <a href="#" class="btn m_btn-get-started">
                                        <span class="btn-get-started__text"></span>
                                        <img src="/dev/images/icon_arrow-inverted.svg" alt="">
                                    </a>
                                </section>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="container fix-little-content-width">
            <div class="o_content-box home_full-height-content-box home_section-invest">
                <div class="row bs-row-full-height">
                    <div class="col-md-1 bs-full-height-column-1">
                        <section class="home_full-height-content-box__heading">
                            <h1>投資</h1>
                            <p style="visibility: hidden;">empty</p>
                        </section>
                    </div>
                    <div class="col-md-11 bs-full-height-column-2">
                        <div class="row">
                            <div class="col-md-4 my--mobile">
                                <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--index"></span><span class="m_heading2__title">大盤即時走勢</span></h2>
                                <!-- Nav tabs -->
                                <ul class="nav nav-pills" role="tablist">
                                    <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">上市</a></li>
                                    <li role="presentation"><a href="#profile1" aria-controls="profile" role="tab" data-toggle="tab">上櫃</a></li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active stock-index" id="home1">
                                        <p class="stock-index__graph"><img src="/dev/images/temp-index.png" style="width: 210px" alt=""></p>
                                        <p class="stock-index__indicator">
                                            <div class="row">
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">開盤</span><span class="color-stock-rise">10773.85</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">昨收</span><span class="color-stock-normal">10723.15</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">最高</span><span class="color-stock-rise">10790.31</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">最低</span><span class="color-stock-decline">10684.32</span></div>
                                            </div>
                                        </p>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade stock-index" id="profile1">
                                        <p class="stock-index__graph"><img src="/dev/images/temp-index.png" style="width: 210px" alt=""></p>
                                        <p class="stock-index__indicator">
                                            <div class="row">
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">開盤</span><span class="color-stock-rise">10773.85</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">昨收</span><span class="color-stock-normal">10723.15</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">最高</span><span class="color-stock-rise">10790.31</span></div>
                                                <div class="col-xs-6"><span class="stock-index__indicator-heading">最低</span><span class="color-stock-decline">10684.32</span></div>
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 my--mobile">
                                <section>
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--stock"></span><span class="m_heading2__title">我的股票</span><a href="#" class="add-more"></a></h2>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>

                                                <th>商品</th>
                                                <th>成交</th>
                                                <th>漲跌</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>

                                                <td>鴻海</td>
                                                <td>105</td>
                                                <td>1.5</td>
                                            </tr>
                                            <tr>

                                                <td>大立光</td>
                                                <td>1023</td>
                                                <td>-0.6</td>
                                            </tr>
                                            <tr>

                                                <td>廣達</td>
                                                <td>41.5</td>
                                                <td>3.3</td>
                                            </tr>

                                            <tr>

                                                <td>微星</td>
                                                <td>63.2</td>
                                                <td>-2.1</td>
                                            </tr>
                                            <tr>

                                                <td>大同</td>
                                                <td>32.6</td>
                                                <td>1.15</td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </section>
                            </div>
                            <div class="col-md-4 my--mobile">
                                <section class="home_my-fund">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--fund"></span><span class="m_heading2__title">我的基金</span><a href="#" class="add-more"></a></h2>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>表現最佳排名</th>
                                                <th>漲跌幅</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>國泰日本ETF傘型之富時日本單日正向2倍</td>
                                                <td>-6.5%</td>
                                            </tr>
                                            <tr>
                                                <td>台新MSCI台灣單日反向1倍</td>
                                                <td>1.5%</td>
                                            </tr>
                                            <tr>
                                                <td>元大台灣50單日反向1倍</td>
                                                <td>-0.7%</td>
                                            </tr>
                                            <tr>
                                                <td>群益臺灣加權單日反向1倍</td>
                                                <td>3.4%</td>
                                            </tr>
                                            <tr>
                                                <td>富邦日本ETF傘型之富邦日本東証單日正向…</td>
                                                <td>-1.2%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </div>
                            <div class="col-md-4 my--mobile">
                                <section>
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--rate"></span><span class="m_heading2__title">銀行利率</span></h2>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>金融機構</th>
                                                <th>定存一年</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>安泰銀行</td>
                                                <td>1.390%</td>
                                            </tr>
                                            <tr>
                                                <td>中國信託</td>
                                                <td>1.070%</td>
                                            </tr>
                                            <tr>
                                                <td>台灣銀行</td>
                                                <td>1.080%</td>
                                            </tr>
                                            <tr>
                                                <td>彰化銀行</td>
                                                <td>1.050%</td>
                                            </tr>
                                            <tr>
                                                <td>合作金庫</td>
                                                <td>1.403%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </div>
                            <div class="col-md-6">

                                <section>
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--exchange"></span><span class="m_heading2__title">外匯</span></h2>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>名稱</th>
                                                <th>最新價</th>
                                                <th>漲跌幅</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>美元/台幣</td>
                                                <td>30.217</td>
                                                <td>0.390%</td>
                                            </tr>
                                            <tr>

                                                <td>美元/港幣</td>
                                                <td>7.817</td>
                                                <td>-1.095%</td>
                                            </tr>
                                            <tr>
                                                <td>美元/人民幣</td>
                                                <td>6.6162</td>
                                                <td>1.100%</td>
                                            </tr>
                                            <tr>
                                                <td>美元/日圓</td>
                                                <td>111.567</td>
                                                <td>0.105%</td>
                                            </tr>
                                            <tr>
                                                <td>歐元/美元</td>
                                                <td>1.1845</td>
                                                <td>-0.080%</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </div>
                            <div class="col-md-2 px-0">
                                <section class="instant-experience">
                                    <h2 class="m_heading2"><span class="division--vertical"></span><span class="icon icon--lightning"></span><span class="m_heading2__title">立即體驗</span></h2>
                                    <a href="#" class="btn m_btn-get-started">
                                        <span class="btn-get-started__text"></span>
                                        <img src="/dev/images/icon_arrow-inverted.svg" alt="">
                                    </a>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container fix-little-content-width">
            <div class="o_content-box home_celebrity-column clearfix">
                <h1 class="m_heading1"><span class="icon icon--celebrity"></span><span class="m_heading1__title">Money名人專欄</span><span class="division--vertical invisible"></span><span class="m_heading1__title--english invisible">Celebrity Column</span><a href="#" class="content-more">更多 <i>&raquo;</i></a></h1>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-7 five-three ">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="m_celebrity__item">
                                        <img class="img-circle" src="/dev/images/img_thumbnail-celebrity1.jpg" alt="">
                                        <h2 class="celebrity__name text-center">童再興</h2>
                                        <p class="celebrity__description">政大政治系40期畢業(1980年)，政大公行所碩士，曾任錢雜誌發行人兼社長</p>
                                        <hr>
                                        <a href="#">
                                            <h3 class="celebrity__article JQellipsis-article">台股這次萬點行情不一樣萬點行情不一萬點行情不一萬點行情不一</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="m_celebrity__item">
                                        <img class="img-circle" src="/dev/images/img_thumbnail-celebrity2.jpg" alt="">
                                        <h2 class="celebrity__name text-center">邱正弘</h2>
                                        <p class="celebrity__description">宏觀財務顧問總經理，東吳大學財金系助理教授</p>
                                        <hr>
                                        <a href="#">
                                            <h3 class="celebrity__article JQellipsis-article">金融科技的下一步</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="m_celebrity__item">
                                        <img class="img-circle" src="/dev/images/img_thumbnail-celebrity3.jpg" alt="">
                                        <h2 class="celebrity__name text-center">李烈</h2>
                                        <p class="celebrity__description">畢業於世界新聞專科學校（現為世新大學）編輯採訪科，資深電影監製</p>
                                        <hr>
                                        <a href="#">
                                            <h3 class="celebrity__article JQellipsis-article">億萬票房監製李烈登錄興櫃是台灣電影必走的路</h3>
                                        </a>
                                    </div>
                                </div>
                                <!-- end inner row -->
                            </div>
                        </div>
                        <div class="col-sm-5 five-two">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="m_celebrity__item hidden-xs">
                                        <img class="img-circle" src="/dev/images/img_thumbnail-celebrity4.jpg" alt="">
                                        <h2 class="celebrity__name text-center">馬克·墨比爾斯</h2>
                                        <p class="celebrity__description">新興市場投資教父，富蘭克林坦伯頓基金集團執行副總</p>
                                        <hr>
                                        <a href="#">
                                            <h3 class="celebrity__article JQellipsis-article">流淚播種才能歡喜收割，墨比爾斯最看好中國及雙印</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="m_celebrity__item hidden-xs">
                                        <img class="img-circle" src="/dev/images/img_thumbnail-celebrity5.jpg" alt="">
                                        <h2 class="celebrity__name text-center">邱志昌</h2>
                                        <p class="celebrity__description">大學助理教授，並長年擔任鉅亨網總主筆，專注研究全球總體經濟發展</p>
                                        <hr>
                                        <a href="#">
                                            <h3 class="celebrity__article JQellipsis-article">ETF投資的2大致勝關鍵</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- end inner row -->
                        </div>
                    </div>
                    <!-- end outer row -->
                </div>
            </div>
        </div>

        <div class="container fix-little-content-width">
            <div class="o_content-box">
                <h1 class="m_heading1"><span class="icon icon--download"></span><span class="m_heading1__title">Money產品下載專區</span><span class="division--vertical invisible"></span><span class="m_heading1__title--english invisible">Download Apps</span></h1>
                <div class="row">
                    <div class="col-sm-4">
                        <h2 class="m_heading2--product money-mgmt"><span class="icon-dot"></span><span class="m_heading2__title">理財</span></h2>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="m_money-product__item">
                                    <a href="#">
                                    <img class="img-responsive" src="/dev/images/icon_product-cwmoney.png" alt="">
                                    </a>
                                    <p class="m_money-product__description">CWMoney</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="m_money-product__item">
                                    <a href="#">
                                    <img class="img-responsive" src="/dev/images/icon_product-receipt-collector.png" alt="">
                                    </a>
                                    <p class="m_money-product__description">中發票集點王</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="m_heading2--product insurance"><span class="icon-dot"></span><span class="m_heading2__title">保險</span></h2>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="m_money-product__item">
                                    <a href="">
                                    <img class="img-responsive" src="/dev/images/icon_product-insurance-right.png" alt="">
                                    </a>
                                    <p class="m_money-product__description">保險對夠好</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="m_heading2--product invest"><span class="icon-dot"></span><span class="m_heading2__title">投資</span></h2>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="m_money-product__item">
                                    <a href="">
                                    <img class="img-responsive" src="/dev/images/icon_product-stock-emily.png" alt="">
                                    </a>
                                    <p class="m_money-product__description">艾蜜莉定存股</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="m_money-product__item">
                                    <a href="">
                                    <img class="img-responsive" src="/dev/images/icon_product-funds-machine.jpg" alt="">
                                    </a>
                                    <p class="m_money-product__description">基金機器人A寶</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <!-- endbuild -->
</body>

</html>
