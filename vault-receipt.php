<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Money錢管家 - 最好的智能理財一站式平台</title>
    <!-- build:css css/styles.min.css -->
    <link rel="stylesheet" href="dev/css/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!-- endbuild -->
</head>

<body id="vault-track-spending">
    <div class="sticky-content">
        <?php 
//            include "dev/php/header-is-not-vip.php";  
              include "dev/php/header-is-vip.php"; 
        ?>
        <div class="container fix-little-content-width">
            <div class="row login_first-row">
                <div class="col-md-12">
                    <div class="o_content-box">
                        <section>
                            <header class="account-detail">
                                <h1 class="m_heading1"><span class="icon icon--receipt-lottery"></span><span class="m_heading1__title">發票</span><a href="#" class="icon icon--info" data-toggle="tooltip" data-placement="right" title="資產總覽是全部所有的資產概況，包含總資產，總負債，淨資產，當月預算，以及當月收支月檢視"></a></h1>
                                <div class="total-asset-stat">
                                    <a href="#" class="btn btn-receipt" role="button">查看中獎號碼</a>
                                </div>
                            </header>
                            <h2 class="text-center receipt-month-switcher">
                                <span class="backwards"><span class="icon icon--backwards"></span></span>
                                <span class="current-months">2017年11-12月</span>
                                <span class="forwards"><span class="icon icon--forwards"></span></span>
                            </h2>

                            <div class="current-receipt-content-container">
                                <p class="text-center receipt-total">共 <span>352</span> 張，總金額 <span>1320</span> 元</p>
                                <p class="text-center receipt-lottery-date">即將於 <span>2017/11/25</span> 開獎</p>
                                <p class="text-center receipt-lottery-date expire">已過領獎期限</p>
                                <div class="alert alert-lottery-win alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>恭喜!</strong> 有 <span>2</span> 張中獎共<span> 2,200,000元</span>！請儘速兌獎！<a href="#" class="alert-link">前往查看</a>
                                </div>

                                <div class="alert alert-lottery-miss alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>很遺憾!</strong> 共有 <span>100</span> 張中獎！已過領獎期限，下次請提早兌獎！
                                </div>
                                <div class="flex-horizontal">

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-default btn-ghost" style="margin-bottom: 10px;" data-toggle="modal" data-target="#receipt-manual-input"><span class="icon-receipt-keyin"></span>手動輸入</button>

                                    <!-- Modal -->
                                    <div class="modal fade slide-from-btm" id="receipt-manual-input" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                                <div class="track-spending--mobile">

                                                    <fieldset>
                                                        <legend style="text-align: center;">電子發票證明聯</legend>
                                                        <form class="form-horizontal track-spending-input my-3 fix-modal-offset">
                                                            <div class="form-group text-center">
                                                                <time class="receipt-month"><span>106</span>年<span>09</span>-<span>10</span>月</time>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="receipt-num-english" class="col-md-2 control-label">發票號碼:</label>
                                                                <div class="col-md-10 flex-horizontal">
                                                                    <input type="text" style="width: 30%;" class="form-control" id="receipt-num-english" aria-labelledby="receipt-num" placeholder="2位英文">
                                                                    <span class="dash">&mdash;</span>
                                                                    <label for="receipt-num-number" class="hidden"></label>
                                                                    <input type="text" style="width: 60%;" class="form-control" id="receipt-num-number" aria-labelledby="receipt-num" placeholder="8位數字">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="spend-money" class="col-md-2 control-label">消費日期:</label>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" id="spend-money" placeholder="請輸入日期">
                                                                </div>
                                                            </div>
                                                            <div class="form-group my-0-mobile">
                                                                <label for="spend-category" class="col-md-2 control-label">隨機碼:</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control random-code" id="spend-category" placeholder="4位數字">
                                                                </div>
                                                                <label for="spend-category" class="col-md-2 control-label">總計:</label>
                                                                <div class="col-md-4">
                                                                    <span class="form-control total">$100</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" style="margin:0px">
                                                                <img src="/dev/images/img_barcode.png" class="img-barcode" alt="">
                                                            </div>
                                                            <div class="form-group flex-horizontal" style="margin:0px">
                                                                <img src="/dev/images/img_app-fa--android.png" alt="">
                                                                <img src="/dev/images/img_app-fa--ios.png" alt="">

                                                            </div>
                                                            <div class="form-group">
                                                                <p class="text-center receipt-meta">退貨憑電子發票證明聯正本辦理</p>
                                                                <p class="text-center receipt-info">本明細為模擬畫面僅供參考</p>
                                                            </div>
                                                            <div class="form-group text-center">
                                                                <button type="submit" class="btn btn-track-spending" style="width: 70%;">儲存再記一筆</button>
                                                            </div>
                                                        </form>

                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-search receipt">
                                        <input type="text" class="form-control" placeholder="輸入品名">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-primary" type="button"><span class="icon icon--search" style="position: relative; top: -1px;"></span></button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </div>

                                <div class="table-responsive scrolling-hints">
                                    <table class="table table-striped account-detail-table receipt" style="position: relative;">
                                        <thead>
                                            <tr style="cursor: initial;">
                                                <th>
                                                    <a href="#" class="table-filter">狀態</a>
                                                </th>
                                                <th>
                                                    <a href="#" class="table-filter">時間</a>
                                                </th>
                                                <th>
                                                    <a href="#" class="table-filter">發票</a>
                                                </th>
                                                <th style="text-align: center;">
                                                    <a href="#" class="table-filter">金額</a>
                                                </th>
                                                <th>
                                                    <a href="#" class="table-filter">記帳</a>
                                                </th>
                                                <th>
                                                    <a href="#" class="table-filter">商家</a>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_no@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678 <img class="img-barcode--mobile" src="/dev/images/img_barcode--mobile.png" alt="手機條碼"></span><br>
                                                    <span class="text-ellipsis longer">御茶園特上紅茶</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$25</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">三商巧福</td>
                                            </tr>
                                            <tr class="dropdown-tablerow">
                                                <td colspan="6">
                                                    <time datetime="2017-08-31">2017/08/31 12:20:37</time>
                                                    <div class="address-n-barcode-container">
                                                        <address style="margin-bottom: 0;"><a href="#"><span class="icon icon--map"></span>三商巧福新埔店</a></address>
                                                        <span class="barcode--mobile"><img class="img-barcode--mobile" src="/dev/images/img_barcode--mobile.png" alt="手機條碼">手機條碼：/AB1234</span>
                                                    </div>
                                                    <table class="table">
                                                        <thead>
                                                            <th>品名(備註）</th>
                                                            <th>數量</th>
                                                            <th>小計(金額)</th>
                                                            <th>帳戶</th>
                                                            <th>分類</th>
                                                            <th>子分類</th>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>御茶園特上紅茶</td>
                                                                <td>1</td>
                                                                <td class="table-num-align-center-right">
                                                                    <div>$200</div>
                                                                </td>
                                                                <td>
                                                                     <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">現金</option>
                                                                        <option value="2">銀行簽帳卡</option>
                                                                        <option value="3">金融卡-銀行帳戶</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">電子發票-分類1</option>
                                                                        <option value="2">電子發票-分類2</option>
                                                                        <option value="3">電子發票-分類3</option>
                                                                        <option value="4">電子發票-分類4</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">電子發票-分類1</option>
                                                                        <option value="2">電子發票-分類2</option>
                                                                        <option value="3">電子發票-分類3</option>
                                                                        <option value="4">電子發票-分類4</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>滷味</td>
                                                                <td>1</td>
                                                                <td class="table-num-align-center-right">
                                                                    <div>$32,000</div>
                                                                </td>
                                                                <td>
                                                                      <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">現金</option>
                                                                        <option value="2">銀行簽帳卡</option>
                                                                        <option value="3">金融卡-銀行帳戶</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                   <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">電子發票-分類1</option>
                                                                        <option value="2">電子發票-分類2</option>
                                                                        <option value="3">電子發票-分類3</option>
                                                                        <option value="4">電子發票-分類4</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <!--  或參考富邦銀行下拉select https://www.fubon.com/banking/personal/index.htm  -->
                                                                    <select class="selectpicker">
                                                                        <option value="1" selected="selected">電子發票-分類1</option>
                                                                        <option value="2">電子發票-分類2</option>
                                                                        <option value="3">電子發票-分類3</option>
                                                                        <option value="4">電子發票-分類4</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="dropdown-tablerow__btn-container">
                                                        <button type="button" class="btn btn-primary">確認記帳</button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_not-yet@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">心態致勝 全新成功心理學</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$240</div>
                                                </td>
                                                <td class=""><span class="is-track-spending">已記帳</span></td>
                                                <td class="">誠品</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_past@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">Fintech金融科技聖經</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$360</div>
                                                </td>
                                                <td class=""><span class="is-track-spending">已記帳</span></td>
                                                <td class="">7-Eleven</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes1.1@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes1.2@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes1@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes3@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678<img class="img-barcode--mobile" src="/dev/images/img_barcode--mobile.png" alt="手機條碼"></span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-track-spending">已記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes4@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes5@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>
                                            <tr>
                                                <td><img src="/dev/images/icon_state_yes6@3x.png" height="44px" alt=""></td>
                                                <td style="vertical-align: middle;">09/29 (二)</td>
                                                <td>
                                                    <span>AB12345678</span><br>
                                                    <span class="text-ellipsis longer">10000份Money錢雜誌</span>
                                                </td>
                                                <td class="table-num-align-center-right">
                                                    <div>$10,000,000</div>
                                                </td>
                                                <td class=""><span class="is-not-track-spending">尚未記帳</span></td>
                                                <td class="">博客來網路書店</td>
                                            </tr>



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <nav aria-label="Page navigation" class="text-right">
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
                                    </li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
                                    </li>
                                </ul>
                            </nav>
                        </section>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /. sticky-content -->

    <?php include "dev/php/footer.php"; ?>

    <!-- build:js js/jquery.min.js -->
    <script src="dev/js/bootstrap/jquery.js"></script>
    <!-- endbuild -->
    <!-- build:js js/bootstrap.min.js -->
    <script src="dev/js/bootstrap/affix.js"></script>
    <script src="dev/js/bootstrap/transition.js"></script>
    <script src="dev/js/bootstrap/tooltip.js"></script>
    <script src="dev/js/bootstrap/alert.js"></script>
    <script src="dev/js/bootstrap/button.js"></script>
    <script src="dev/js/bootstrap/carousel.js"></script>
    <script src="dev/js/bootstrap/collapse.js"></script>
    <script src="dev/js/bootstrap/dropdown.js"></script>
    <script src="dev/js/bootstrap/modal.js"></script>
    <script src="dev/js/bootstrap/popover.js"></script>
    <script src="dev/js/bootstrap/scrollspy.js"></script>
    <script src="dev/js/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <!-- build:js js/myscript.min.js -->
    <script src="dev/js/modules/myscript-1.js"></script>
    <script src="dev/js/modules/myscript-2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- endbuild -->
</body>

</html>
